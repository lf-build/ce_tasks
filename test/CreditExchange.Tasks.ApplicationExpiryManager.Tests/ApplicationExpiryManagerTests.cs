﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using LendFoundry.Foundation.Logging;
using Xunit;
using LendFoundry.Security.Tokens;
using LendFoundry.Configuration.Client;
using CreditExchange.Applications.Filters;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ApplicationExpiryManager.Tests
{
    public class ApplicationExpiryManagerTests : InMemoryObjects
    {
        public ApplicationExpiryManagerTests()
        {
            ConfigurationFactory.Setup(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);
            EventHubClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(EventHubClient.Object);
            CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(CalendarService.Object);
            StatusManagementServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
              .Returns(StatusManagementService.Object);

            ApplicationFilterServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationFilterService.Object);

            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);
        }

        private Agent Agent => new Agent
        (
            ConfigurationFactory.Object,
            TokenHandler.Object,
            EventHubClientFactory.Object,
            TenantTimeFactory.Object,
            StatusManagementServiceFactory.Object,
            ApplicationFilterServiceFactory.Object,
            LoggerFactory.Object
        );

        private Mock<ISettingReader> SettingReader { get; } = new Mock<ISettingReader>();

        [Fact]
        public  void OnSchedule_Test()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));
            
            ConfigurationService.Setup(x => x.Get())
                .Returns(new ApplicationExpiryManager.Configuration
                {
                    Expiration = new ApplicationExpiryManager.ExpirationMapping
                    {
                        StatusCode ="500.01"
                    }
                });
           
            ApplicationFilterService.Setup(x => x.GetAllExpiredApplications())
                .Returns(Task.FromResult(new List<IFilterView>
                {
                    GetData()
                }.AsEnumerable()));

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
           
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

           // TenantTime.Verify(x => x.Today);
         
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            ApplicationFilterService.Verify(x => x.GetAllExpiredApplications(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeast(3));
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

       

        [Fact]
        public void OnSchedule_When_HasNoFound_UpdateStatusConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

            ConfigurationService.Setup(x => x.Get());

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
          
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<ApplicationExpiryManager.Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

          //  TenantTime.Verify(x => x.Today);
           
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_UpdateExpiredStatusConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

         

            ConfigurationService.Setup(x => x.Get())
                .Returns(new ApplicationExpiryManager.Configuration
                {
                    Expiration = new ExpirationMapping()
                });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
          
            ApplicationFilterServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<ApplicationExpiryManager.Configuration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

         //   TenantTime.Verify(x => x.Today);
           
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }


        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);

            //program.Main(null);
        }

        private IFilterView GetData()
        {
            return new FilterView
            {
                ApplicationId = "application001",
                Applicant = "applicant001",
                AmountRequested = 40000,
                FinalOfferAmount = 30000,                
                SourceId = "source001",
                SourceType = "merchant",
                StatusCode = "R",
                StatusName = "Rejected",
                Submitted = DateTime.Now,
                ExpirationDate = DateTime.Now.AddDays(30)
            };
        }
    }
}
