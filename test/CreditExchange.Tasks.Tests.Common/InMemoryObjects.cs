﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Moq;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.Tests.Common
{
    public abstract class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        // ------------------------- Configuration Objects
        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();

        // ------------------------- TenantTime Objects
        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();

        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        
        // ------------------------- Calendar Objects
        protected Mock<ICalendarService> CalendarService { get; } = new Mock<ICalendarService>();
        protected Mock<ICalendarServiceFactory> CalendarServiceFactory { get; } = new Mock<ICalendarServiceFactory>();
        
        // ------------------------- Logger Objects
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        // ------------------------- Ach Objects
    }
}