﻿namespace CreditExchange.Tasks.ReportStatusSummary
{
    public class CCAddress
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}