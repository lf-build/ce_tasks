using System.Collections.Generic;

namespace CreditExchange.Tasks.ReportStatusSummary
{
    public class Configuration
    {
        public List<ToAddress> toaddressforutmreport { get; set; }
        public List<CCAddress> ccaddressforutmreport { get; set; }
        public string nameforutmreport { get; set; }
        public string nameforcount { get; set; }
        public List<ToAddress> toaddressforstatus { get; set; }
        public List<CCAddress> ccaddressforstatus { get; set; }
    }
}