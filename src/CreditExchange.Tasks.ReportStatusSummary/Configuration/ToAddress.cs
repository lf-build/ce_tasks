﻿namespace CreditExchange.Tasks.ReportStatusSummary
{
    public class ToAddress
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}