using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Tasks.Agent;
using System;
using System.Linq;
using LendFoundry.Email.Client;
using LendFoundry.Email;
using System.IO;
using System.Collections.Generic;
using CreditExchange.Applications.Filters;
using CsvHelper;
using System.Globalization;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ReportStatusSummary
{
    public class Agent : ScheduledAgent
    {
        #region Constructor

        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IApplicationFilterClientServiceFactory applicationFilterClientFactory,
            IEmailServiceFactory emailSerivceFactory,
            ILoggerFactory loggerFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            ApplicationFilterClientFactory = applicationFilterClientFactory;
            EmailServiceFactory = emailSerivceFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
        }

        #endregion

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IApplicationFilterClientServiceFactory ApplicationFilterClientFactory { get; }
        private IEmailServiceFactory EmailServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }

        #endregion

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName, DateTime.Now.AddHours(1), "AutoExpiryTask", new string[] { });
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);

            var applicationFilter = ApplicationFilterClientFactory.Create(reader);
            var emailservice = EmailServiceFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);

            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to application filter expiry status Task");
                return;
            }

            var reportstatussummary = configuration.Get();
            if (reportstatussummary == null)
            {
                logger.Error("Was not found the Configuration related to application expiry manager");
                return;
            }

            try
            {
                var statusCount = await applicationFilter.GetReportStatusCount();
                if (statusCount != null)
                {
                    var i = 1;
                    var textInfo = new CultureInfo("en-US", false).TextInfo;
                    var listCount = statusCount.Select(p => new ListCount
                    {
                        index = i++,
                        Title = textInfo.ToTitleCase(p.Key ?? string.Empty),
                        DataValue = p.Value
                    }).ToList();

                    var total = listCount.Sum(p => p.DataValue);
                    object toaddressforstatus = reportstatussummary.toaddressforstatus;
                    object ccaddressforstatus = reportstatussummary.ccaddressforstatus;
                    var date = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                    var EmailPayload = new { firstname = reportstatussummary.nameforcount, ToAddress = toaddressforstatus, CcAddress = ccaddressforstatus, todate = date, ListCount = listCount, totalcount = total };
                    var emaiStatusCountResult = await emailservice.Send("ReportStatusSummary", "1.0", EmailPayload);
                }
                
                var trackingCodeList = await applicationFilter.GetUTMTagReport();
                List<IFilterView> result = trackingCodeList.ToList();
                List<FilterView> filterviewobj = new List<FilterView>();

                foreach (var item in result)
                {
                    filterviewobj.Add(new FilterView()
                    {
                        ApplicationNumber = item.ApplicationNumber,
                        AmountRequested = item.AmountRequested,
                        Applicant = item.Applicant,
                        InitialOfferAmount = item.InitialOfferAmount,
                        FinalOfferAmount = item.FinalOfferAmount,
                        TrackingCode = item.TrackingCode,
                        StatusName = item.StatusName
                    });
                }

                var filterviewtoByte = WriteToCsv(filterviewobj);
                Stream stream = new MemoryStream(filterviewtoByte);

                List<EmailAttachmentDetails> emaildetails = new List<EmailAttachmentDetails>();
                var ReportDate = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                emaildetails.Add(new EmailAttachmentDetails()
                {
                    Content = stream,
                    FileName = "UTM_tagreport" + ReportDate + ".csv"
                });
                object toaddressforutmreport = reportstatussummary.toaddressforutmreport;
                object ccaddressforutmreport = reportstatussummary.ccaddressforutmreport;

                var EmailUTMPayload = new { firstname = reportstatussummary.nameforutmreport, ToAddress =  toaddressforutmreport, CcAddress = ccaddressforutmreport, todate = ReportDate };
                var emailUtmResult = await emailservice.Send("UTMTagReportSummary", "1.0", EmailUTMPayload, emaildetails.AsEnumerable());

            }
            catch (Exception ex)
            {
                logger.Error($"Failed attempt to update application status. Application Filter service not started yet.", ex);
            }
        }

        private class ListCount
        {
            public int index { get; set; }
            public string Title { get; set; }
            public int DataValue { get; set; }
        }

        private byte[] WriteToCsv<T>(IEnumerable<T> items)
        {
            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream))
                    using (var csvWriter = new CsvWriter(streamWriter))
                    {
                        csvWriter.WriteRecords(items);
                    }
                    return memoryStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}