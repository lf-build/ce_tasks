using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.ReportStatusSummary
{
    public static class Settings
    {
        public static string ServiceName { get; } = "task-reportstatussummary";
        public static string Prefix = ServiceName.ToUpper();
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings ApplicationFilter { get; } = new ServiceSettings($"{Prefix}_APPLICATIONFILTER", "application-filters");
        public static ServiceSettings Email { get; } = new ServiceSettings($"{Prefix}_EMAIL", "email");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
    }
}