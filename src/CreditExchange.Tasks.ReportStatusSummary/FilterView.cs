﻿namespace CreditExchange.Tasks.ReportStatusSummary
{
    public class FilterView
    {
        public string ApplicationNumber { get; set; }
        public string Applicant { get; set; }
        public double AmountRequested { get; set; }
        public double FinalOfferAmount { get; set; }
        public double InitialOfferAmount { get; set; }
        public string TrackingCode { get; set; }
        public string StatusName { get; set; }
    }
}