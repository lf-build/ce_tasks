using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Tasks.Dray.Client
{
    public class Job
    {
        public Job()
        {
            Steps = new List<Step>();
        }

        private Job(string name) : this()
        {
            Name = name;
        }

        public string Id { get; set; }

        public string Name { get; set; }
        
        public List<Step> Steps { get; set; }

        public static Job Parse(ITask task)
        {
            var job = new Job(task.Name);
            var step = new Step(task.Name)
            {
                Source = task.Image,
                Environment = task.Environment
                    .Select(pair => new EnvironmentVariable(pair.Key, pair.Value))
                    .ToList()
            };
            step.Environment.Add(new EnvironmentVariable("TASK_TENANT", task.Tenant));
            job.Steps.Add(step);
            return job;
        }
    }
}