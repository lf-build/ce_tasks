using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

using CreditExchange.Tasks.Agent;
using System;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.Zumigo.Client;
using CreditExchange.Syndication.Zumigo;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ProcessRetryZumigo
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IZumigoServiceClientFactory zumigoServiceClientFactory,
            ILoggerFactory loggerFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
            ZumigoServiceClientFactory = zumigoServiceClientFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }
     
        private IZumigoServiceClientFactory ZumigoServiceClientFactory { get; }

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var zumigoService =ZumigoServiceClientFactory.Create(reader);
            try
            {
                logger.Info("Start Zumigo Retry task");
                var retryRequest =await zumigoService.GetRetryRequest();
                if (retryRequest != null && retryRequest.Count > 0)
                {
                    foreach (var item in retryRequest)
                    {
                        if(item.Request.IsRetryDistance == true)
                        await zumigoService.VerifyDeviceLocation(item.EntityType, item.EntityId, item.Request.MobileNumber,item.Request);
                    }
                }

                logger.Info("End Zumigo Retry task");
            }
            catch (Exception ex)
            {
                logger.Error($"Error While Retrying the Zumigo task", ex);
            }
        }
    }
}