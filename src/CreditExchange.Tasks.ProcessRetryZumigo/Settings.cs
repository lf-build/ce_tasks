using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.ProcessRetryZumigo
{
    public static class Settings
    {
        public static string ServiceName { get; } = "zumigo-retry-task";

        public static string Prefix = "RETRY-ZUMIGO-TASK";
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT_HOST","tenant", $"{Prefix}_TENANT_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings Zumigo { get; } = new ServiceSettings($"{Prefix}_ZUMIGO_HOST", "syndication-zumigo", $"{Prefix}_ZUMIGO_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}