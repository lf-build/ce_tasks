﻿using CreditExchange.Cibil.Client;
using CreditExchange.Tasks.Agent;
using CreditExchange.Syndication.Cibil;
using LendFoundry.Application.Document.Client;
using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Application.Document;
using System.Text;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.SaveCibilHtmlReport
{
    public class SaveCibilHtmlReportAgent : EventBasedAgent
    {
        public SaveCibilHtmlReportAgent
       (
           IConfigurationServiceFactory configurationServiceFactory,
           IEventHubClientFactory eventHubFactory,
           ITokenHandler tokenHandler,
           ITokenReaderFactory tokenHandlerFactory,
           IApplicationDocumentServiceClientFactory applicationDocumentServiceClientFactory,
           ICibilReportServiceClientFactory cibilReportServiceClientFactory,
           ILogger logger
           )
            : base(eventHubFactory, nameof(GetCibilHtmlReportTriggered), tokenHandler, Settings.ServiceName)
        {

            ApplicationDocumentServiceClientFactory = applicationDocumentServiceClientFactory;
            CibilReportServiceClientFactory = cibilReportServiceClientFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            Logger = logger;
        }
        private ICibilReportServiceClientFactory CibilReportServiceClientFactory { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private IApplicationDocumentServiceClientFactory ApplicationDocumentServiceClientFactory { get; }
        private ILogger Logger { get; }

        private ITokenReaderFactory TokenHandlerFactory { get; }

        public override async void Execute(EventInfo @event)
        {
            var data = @event.Cast<GetCibilHtmlReportTriggered>();
            try
            {
                if (data != null)
                {
                    var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var configurationService = ConfigurationServiceFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
                    var configuration = configurationService.Get();
                    if (configuration == null)
                    {
                        Logger.Error($"No configuration found for save Cibil Html report task for tenant #{Tenant}");
                        return;
                    }

                    IApplicationDocumentService applicationdocumentservice = ApplicationDocumentServiceClientFactory.Create(reader);
                    var cibilService = CibilReportServiceClientFactory.Create(reader);
                    Logger.Info("Retrieve Cibil Html Document Requested ");
                    var cibilresponse = cibilService.GetHtmlReport(data.EntityType, data.EntityId).Result;
                    if (cibilresponse != null)
                    {
                        var filename = data.Name + "_CIBILReport.html";
                        var response = await applicationdocumentservice.Add(data.EntityId, configuration.DocumentCategoryName, Encoding.ASCII.GetBytes(cibilresponse.report), filename, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Save Cibil html Document Failed", ex);
            }
        }
    }
}
