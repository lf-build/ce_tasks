﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.ApplicationExpiryManager
{
    public class ExpirationMapping
    {
        public string StatusCode { get; set; }
        public string Reason { get; set; }
        public List<string> ExcludedStatuses { get; set; }
    }
}