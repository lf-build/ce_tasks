using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.ApplicationExpiryManager
{
    public static class Settings
    {
        public static string ServiceName { get; } = "task-applicationexpirymanager";
        public static string Prefix = "TASK_APPLICATIONEXPIRYMANAGER";
        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUSMANAGEMENT_HOST", "status-management", $"{Prefix}_STATUSMANAGEMENT_PORT");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings ApplicationFilter { get; } = new ServiceSettings($"{Prefix}_APPLICATIONFILTER_HOST", "application-filters", $"{Prefix}_APPLICATIONFILTER_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}