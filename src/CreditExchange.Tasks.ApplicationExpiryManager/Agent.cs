using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using CreditExchange.Applications.Filters.Client;
using LendFoundry.StatusManagement.Client;
using CreditExchange.Tasks.Agent;
using System;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.StatusManagement;
using System.Collections.Generic;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ApplicationExpiryManager
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IStatusManagementServiceFactory statusManagementServiceFactory,
            IApplicationFilterClientServiceFactory applicationFilterClientFactory,
            ILoggerFactory loggerFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            StatusManagementServiceFactory = statusManagementServiceFactory;
            ApplicationFilterClientFactory = applicationFilterClientFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
        }

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ICalendarServiceFactory CalendarServiceFactory { get; }
        private IStatusManagementServiceFactory StatusManagementServiceFactory { get; }
        private IApplicationFilterClientServiceFactory ApplicationFilterClientFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }

        #endregion

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName, DateTime.Now.AddHours(1), "AutoExpiryTask", new string[] { });
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var statusManagement = StatusManagementServiceFactory.Create(reader);
            IApplicationFilterService applicationFilter = ApplicationFilterClientFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);

            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to application filter expiry status Task");
                return;
            }

            var applicationExpiryManagerConfiguration = configuration.Get();
            if (applicationExpiryManagerConfiguration == null)
            {
                logger.Error("Was not found the Configuration related to application expiry manager");
                return;
            }
            if (applicationExpiryManagerConfiguration.Expiration == null)
            {
                logger.Error("Was not found the Configuration related to application expiry manager status");
                return;
            }
            var expiryStatusCode = applicationExpiryManagerConfiguration.Expiration.StatusCode;
            if (string.IsNullOrWhiteSpace(expiryStatusCode))
            {
                logger.Warn("Was not found any status code.");
                return;
            }
            try
            {
                List<string> status = new List<string>();
                status.Add(expiryStatusCode);
                if (applicationExpiryManagerConfiguration.Expiration.ExcludedStatuses != null && applicationExpiryManagerConfiguration.Expiration.ExcludedStatuses.Count > 0)
                    status.AddRange(applicationExpiryManagerConfiguration.Expiration.ExcludedStatuses);
                var AllExpiredApplications = await applicationFilter.GetExcludedStatusExpired(status);

              //  AllExpiredApplications = AllExpiredApplications.Where(p => p.StatusCode != expiryStatusCode);
                logger.Info("Start expiry manager task");
                if (AllExpiredApplications != null && AllExpiredApplications.Any())
                {
                   
                    logger.Info("Start for each expired application");

                    foreach (var ExpiredApplication in AllExpiredApplications)
                    {
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(ExpiredApplication.ApplicationNumber))
                            {
                                await statusManagement.ChangeStatus("application", ExpiredApplication.ApplicationNumber, expiryStatusCode, new RequestModel { reasons = new System.Collections.Generic.List<string>() { applicationExpiryManagerConfiguration.Expiration.Reason } });
                                await Task.Run(async () => await eventHub.Publish("ApplicationExpired", new { ExpiredApplication = ExpiredApplication }));
                                logger.Info($"Application: {ExpiredApplication.ApplicationNumber} has been successfully updated with status { expiryStatusCode}.");
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error($"Failed attempt to update application status. Application: {ExpiredApplication.ApplicationNumber}\n The status management service is returning this error:", ex);
                        }
                    }

                    //Parallel.ForEach(AllExpiredApplications, ExpiredApplication =>
                    //{
                    //    try
                    //    {
                    //        if (!string.IsNullOrWhiteSpace(ExpiredApplication.ApplicationNumber))
                    //        {
                    //           statusManagement.ChangeStatus("application", ExpiredApplication.ApplicationNumber, expiryStatusCode, new System.Collections.Generic.List<string>() { applicationExpiryManagerConfiguration.Expiration.Reason});
                    //           Task.Run(async () =>  await  eventHub.Publish("ApplicationExpired", new { ExpiredApplication = ExpiredApplication }));
                    //           logger.Info($"Application: {ExpiredApplication.ApplicationNumber} has been successfully updated with status { expiryStatusCode}.");
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        logger.Error($"Failed attempt to update application status. Application: {ExpiredApplication.ApplicationNumber}\n The status management service is returning this error:", ex);
                    //    }
                    //});
                    logger.Info("End for each expired application");
                }
                else
                    logger.Info("No application has been expired yet.");
                logger.Info("end expiry manager task");
            }
            catch (Exception ex)
            {
                logger.Error($"Failed attempt to update application status. Application Filter service not started yet.", ex);
            }
        }
    }
}