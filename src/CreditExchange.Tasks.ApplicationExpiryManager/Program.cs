﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using CreditExchange.Applications.Filters.Client;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Calendar.Client;
using LendFoundry.EventHub.Client;
using System;

namespace CreditExchange.Tasks.ApplicationExpiryManager
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);            
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddApplicationsFilterService(Settings.ApplicationFilter.Host, Settings.ApplicationFilter.Port);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}