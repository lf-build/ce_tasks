using System;
using System.Linq;
using CreditExchange.Tasks.Agent;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.Application.Document.Client;
using CreditExchange.Sftp;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using System.Threading.Tasks;
using CreditExchange.Tasks.SaveFileToAppDocuments.Configuration;
using System.Text.RegularExpressions;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Application.Document;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.SaveFileToAppDocuments
{
    public class SaveFileToAppDocsAgent : ScheduledAgent
    {
        public SaveFileToAppDocsAgent
        (
            IConfigurationServiceFactory configurationFactory,
            ITenantTimeFactory tenantTimeFactory,
            ITokenHandler tokenHandler,
            IApplicationFilterClientServiceFactory appFilterClientServiceFactory,
            IApplicationDocumentServiceClientFactory applicationDocumentServiceClientFactory,
            ILoggerFactory loggerFactory
        ) : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            AppFilterClientServiceFactory = appFilterClientServiceFactory;
            ApplicationDocumentServiceClientFactory = applicationDocumentServiceClientFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IApplicationFilterClientServiceFactory AppFilterClientServiceFactory { get; }
        private IApplicationDocumentServiceClientFactory ApplicationDocumentServiceClientFactory { get; }

        public override void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Now processing documents for Tenant #{Tenant}");
            try
            {
                var ConfigurationService = ConfigurationFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
                var config = ConfigurationService.Get();

                if (config != null && config.FileDownloadConfig != null && config.FileDownloadConfig.Count() > 0)
                {
                    Parallel.ForEach(config.FileDownloadConfig, (currentConfig) =>
                    {
                        logger.Info($"Now processing add document to application documents for category {currentConfig.DocumentCategoryName} from {currentConfig.SftpLocationPath}");
                        var fileService = new SftpService(currentConfig.SftpConfiguration, logger);
                        var applicationFilters = AppFilterClientServiceFactory.Create(reader);
                        var applicationDocumentService = ApplicationDocumentServiceClientFactory.Create(reader);
                        ProcessFileDownload(currentConfig, logger, applicationFilters, applicationDocumentService, fileService);
                    });
                }
                else
                {
                    logger.Error($"Task to add documents to application documents don't have any configuration defined");
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Error occurred while processing files to Application documents from Task {this.GetType()}", ex);
            }
        }

        private void ProcessFileDownload(RepositoryConfig repConfig, ILogger logger, IApplicationFilterService applicationFilters,
                                        IApplicationDocumentService applicationDocumentService, SftpService fileService)
        {
            try
            {
                Regex regex = new Regex(repConfig.PatternToMatch);
                var filesInRepository = fileService.ListFilesInDirectory(repConfig.SftpLocationPath);
                var filesToBeProcessed = filesInRepository.Where(x => regex.IsMatch(x) == true);
                foreach (var file in filesToBeProcessed)
                {
                    logger.Info($"Processing {file} from {repConfig.SftpLocationPath}");
                    var attributeValue = regex.Match(file).Value;
                    var appSnapShot = applicationFilters.SearchApplicationForAttribute(repConfig.PatternPropertyName, attributeValue);
                    string applicationNumber = string.Empty;
                    if (appSnapShot != null)
                    {
                        applicationNumber = appSnapShot.Item1;
                    }
                    
                    if (!string.IsNullOrEmpty(applicationNumber))
                    {
                        var fileData = fileService.DownloadFile(repConfig.SftpLocationPath + file);
                        if (fileData != null && fileData.Length > 0)
                        {
                            applicationDocumentService.Add(applicationNumber, repConfig.DocumentCategoryName, fileData, file, repConfig.Tags.ToList(), null).Wait();
                            fileService.Delete(file, repConfig.SftpLocationPath);
                            logger.Info($"Document processing for {file} completed successfully for application #{applicationNumber}", new { EntityType = "application", EntityId = applicationNumber });
                        }
                        else
                        {
                            logger.Warn($"Processing failed for file : {file} from {repConfig.SftpLocationPath} for {repConfig.DocumentCategoryName}");
                        }
                    }
                    else
                    {
                        logger.Warn($"Processing failed for file : {file} from {repConfig.SftpLocationPath} for {repConfig.DocumentCategoryName} as could not find Application snapshot");                        
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Error occurred while processing {repConfig.SftpLocationPath} for {repConfig.DocumentCategoryName}", ex);
            }
        }
    }
}
