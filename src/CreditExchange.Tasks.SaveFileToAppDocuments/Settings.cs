using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.SaveFileToAppDocuments
{
    public static class Settings
    {
        public static string ServiceName { get; } = "save-doc-to-appdocs";

        public static string Prefix = ServiceName.ToUpper();

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings ApplicationFilter { get; } = new ServiceSettings($"{Prefix}_APPLICATIONFILTER_HOST", "application-filters", $"{Prefix}_APPLICATIONFILTER_PORT");
        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings($"{Prefix}_APPLICATION_DOCUMENT_HOST", "ce-application-document", $"{Prefix}_APPLICATION_DOCUMENT_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";

    }
}