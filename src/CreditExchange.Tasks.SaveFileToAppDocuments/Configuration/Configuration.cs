﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.SaveFileToAppDocuments.Configuration
{
    public class Configuration
    {
        public IEnumerable<RepositoryConfig> FileDownloadConfig { get; set; }        
    }
}
