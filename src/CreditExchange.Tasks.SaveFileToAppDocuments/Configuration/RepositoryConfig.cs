﻿using CreditExchange.Sftp.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.SaveFileToAppDocuments.Configuration
{
    public class RepositoryConfig
    {
        public SftpProviderConfiguration SftpConfiguration { get; set; }
        public string SftpLocationPath { get; set; }
        public string PatternToMatch { get; set; }
        public string PatternPropertyName { get; set; }
        public string DocumentCategoryName { get; set; }
        public IEnumerable<string> Tags { get; set; }     
    }
}
