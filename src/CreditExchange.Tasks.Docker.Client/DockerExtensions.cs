using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Tasks.Docker.Client
{
    public static class DockerExtensions
    {
        public static IServiceCollection AddDockerRunner(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ITaskRunnerFactory>(p => new DockerRunnerFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITaskRunnerFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}