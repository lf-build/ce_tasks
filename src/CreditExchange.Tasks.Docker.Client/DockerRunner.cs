﻿using System;
using System.Threading.Tasks;
using RestSharp;
using System.Linq;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Tasks.Docker.Client
{
    public class DockerRunner : ITaskRunner
    {
        public DockerRunner(IServiceClient client)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            Client = client;
        }

        private IServiceClient Client { get; }

        public Task<bool> Run(ITask task)
        {
            var request = new RestRequest("/containers", Method.PUT);
            request.AddJsonBody(new
            {
                task.Image,
                task.Environment,
                Links = task.Links.Select(p => $"{p.Key}:{p.Value}").ToList()
            });
            return Client.ExecuteAsync(request);
        }
    }
}
