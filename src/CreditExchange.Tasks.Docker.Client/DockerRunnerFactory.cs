using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;

namespace CreditExchange.Tasks.Docker.Client
{
    public class DockerRunnerFactory : ITaskRunnerFactory
    {
        public DockerRunnerFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ITaskRunner Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new DockerRunner(client);
        }
    }
}