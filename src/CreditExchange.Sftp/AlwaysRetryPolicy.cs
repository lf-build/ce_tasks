﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Sftp
{
    public class AlwaysRetryPolicy : IRecoverabilityPolicy
    {
        public bool CanBeRecoveredFrom(Exception ex)
        {
            return true;
        }
    }
}
