﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LendFoundry.Foundation.Logging;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using CreditExchange.Sftp.Configuration;

namespace CreditExchange.Sftp
{
    public class SftpService : IFileStorageService
    {
        public SftpService(ISftpProviderConfiguration sftpProviderConfiguraion, ILogger logger)
        {
            if (sftpProviderConfiguraion == null)
                throw new ArgumentNullException(nameof(sftpProviderConfiguraion));

            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            Logger = logger;
            _sftpProviderConfiguraion = sftpProviderConfiguraion;
        }
        private ILogger Logger { get; set; }

        private readonly ISftpProviderConfiguration _sftpProviderConfiguraion;

        public bool IsExist(string fileName, string destinationPath)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("Argument is null or whitespace", nameof(fileName));

            if (string.IsNullOrWhiteSpace(destinationPath))
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                return client.Exists(destinationPath + fileName);
            }
        }

        public void Upload(string fileFullName, string destinationPath)
        {
            if (string.IsNullOrWhiteSpace(fileFullName))
                throw new ArgumentException("Argument is null or whitespace", nameof(fileFullName));
            if (string.IsNullOrWhiteSpace(destinationPath))
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));

            using (var client = GetSftpClient())
            {
                if (client == null)
                {
                    throw new InvalidOperationException("Sftp Client is not initialized");
                }

                FaultRetry.RunWithAlwaysRetry(client.Connect);
                var destination = Path.Combine(destinationPath, Path.GetFileName(fileFullName));
                FaultRetry.RunWithAlwaysRetry(
                    () => client.UploadFile(File.OpenRead(fileFullName), destination, true));
            }
        }

        public void Upload(Stream stream, string fileName, string destinationPath)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("Argument is null or whitespace", nameof(fileName));
            if (string.IsNullOrWhiteSpace(destinationPath))
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));

            using (var client = GetSftpClient())
            {
                if (client == null)
                {
                    throw new InvalidOperationException("Sftp Client is not initialized");
                }

                FaultRetry.RunWithAlwaysRetry(client.Connect);
                var destination = Path.Combine(destinationPath, fileName);
                Logger.Info($"trying to write file : {destination}");
                FaultRetry.RunWithAlwaysRetry(
                    () => client.UploadFile(stream, destination, true));
            }
        }

        private SftpClient GetSftpClient()
        {
            return new SftpClient(_sftpProviderConfiguraion.Host, _sftpProviderConfiguraion.Port,
                _sftpProviderConfiguraion.Username, _sftpProviderConfiguraion.Password);
        }

        public List<string> DownloadFiles(string sourcePath, string destinationPath)
        {
            if (string.IsNullOrWhiteSpace(sourcePath))
                throw new ArgumentException("Argument is null or whitespace", nameof(sourcePath));

            if (string.IsNullOrWhiteSpace(destinationPath))
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));

            var fileNameList = new List<string>();
            var inboxLocation = sourcePath;

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                foreach (var file in client.ListDirectory(inboxLocation))
                {
                    if (file.IsRegularFile)
                    {
                        try
                        {
                            FaultRetry.RunWithAlwaysRetry(
                                () =>
                                {
                                    using (var fs = new FileStream(destinationPath + file.Name, FileMode.Create))
                                    {
                                        client.DownloadFile(file.FullName, fs);
                                        fs.Close();
                                    }
                                });

                            fileNameList.Add(file.Name);
                        }
                        catch (Exception exception)
                        {
                            Logger.Error("Unable to download file", exception);
                            // ignored
                        }
                    }
                }
            }

            return fileNameList;
        }

        public byte[] DownloadFile(string filePath)
        {
            byte[] fileData;    
            MemoryStream fs = new MemoryStream();
            try
            {
                using (var client = GetSftpClient())
                {
                    client.Connect();
                    client.DownloadFile(filePath, fs);
                    fileData = fs.ToArray();
                    fs.Close();
                    client.Disconnect();
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Unable to download file", exception);
                throw exception;
            }
            return fileData;     
        }

        public void Delete(string fileName, string destinationPath)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("Argument is null or whitespace", nameof(fileName));

            if (string.IsNullOrWhiteSpace(destinationPath))
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                client.DeleteFile(destinationPath + fileName);
            }
        }

        public IEnumerable<string> ListFilesInDirectory(string destinationPath)
        {
            if (string.IsNullOrWhiteSpace(destinationPath))
                throw new ArgumentException("Argument is null or whitespace", nameof(destinationPath));

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                var allInDirectory = client.ListDirectory(destinationPath);

                return allInDirectory?.Where(x => x.IsDirectory == false).Select(x => x.Name);
            }
        }
    }
}
