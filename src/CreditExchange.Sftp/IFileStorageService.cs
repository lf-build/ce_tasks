﻿using Renci.SshNet.Sftp;
using System.Collections.Generic;
using System.IO;

namespace CreditExchange.Sftp
{
    public interface IFileStorageService
    {
        void Upload(string fileFullName, string destinationPath);
        void Upload(Stream stream, string fileName, string destinationPath);
        bool IsExist(string fileName, string outboxLocation);
        List<string> DownloadFiles(string sourcePath, string destinationPath);      
        void Delete(string fileName, string outboxLocation);
        IEnumerable<string> ListFilesInDirectory(string destinationPath);
        byte[] DownloadFile(string filePath);
    }
}
