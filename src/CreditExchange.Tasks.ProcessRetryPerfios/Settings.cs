using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.ProcessRetryPerfios
{
    public static class Settings
    {
        public static string ServiceName { get; } = "retry-perfios-report";

        public static string Prefix = "RETRY-PERFIOS-REPORT";

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Perfios { get; } = new ServiceSettings($"{Prefix}_PERFIOS_HOST", "ce-syndication-perfios", $"{Prefix}_PERFIOS_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");        
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}