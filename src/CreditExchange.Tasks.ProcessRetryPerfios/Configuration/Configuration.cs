﻿namespace CreditExchange.Tasks.ProcessRetryPerfios.Configuration
{
    public class Configuration
    {
        public RetryTaskConfig PerfiosRetryConfig { get; set; }
    }
}
