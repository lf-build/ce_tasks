﻿namespace CreditExchange.Tasks.ProcessRetryPerfios.Configuration
{
    public class RetryTaskConfig
    {
        public int MaxAttempts { get; set; }
        public int[] AttemptDelay { get; set; }
    }
}
