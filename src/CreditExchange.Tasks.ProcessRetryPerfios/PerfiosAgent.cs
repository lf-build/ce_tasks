using System;
using CreditExchange.Tasks.Agent;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using System.Threading;
using CreditExchange.Tasks.ProcessRetryPerfios.Abstractions;
using CreditExchange.Tasks.ProcessRetryPerfios.Persistence;
using CreditExchange.Perfios.Events;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using CreditExchange.Perfios.Client;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ProcessRetryPerfios
{
    public class PerfiosAgent : EventBasedAgent
    {
        public PerfiosAgent
        (
            IEventHubClientFactory eventHubFactory,
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
             ITokenReaderFactory tokenHandlerFactory,
            ITenantTimeFactory tenantTimeFactory,
            IPerfiosServiceClientFactory perfiosCreditReportServiceFactory,
            ITenantServiceFactory tenantServiceFactory,
            IMongoConfiguration mongoConfiguration,
            ILogger logger
            )
            : base(eventHubFactory, nameof(PerfiosXmlReportFailed), tokenHandler, Settings.ServiceName )
        {
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            PerfiosCreditReportServiceFactory = perfiosCreditReportServiceFactory;
            TenantServiceFactory = tenantServiceFactory;
            MongoConfiguration = mongoConfiguration;
            Logger = logger;       
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IPerfiosServiceClientFactory PerfiosCreditReportServiceFactory { get; }
        private IMongoConfiguration MongoConfiguration { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ILogger Logger { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }
        public override async void Execute(EventInfo @event)
        {
            var reader = TokenHandlerFactory.Create(@event.TenantId, string.Empty);
            var configuration = ConfigurationFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
            var TenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var tenantService = TenantServiceFactory.Create(reader);
            var RetryAuditRespository = new PerfiosRetryAuditRepository(tenantService, MongoConfiguration);

            if (configuration == null)
            {
                Logger.Error("Was not found the Configuration related to Perfios retry Task");
                return;
            }

            var data = @event.Cast<PerfiosXmlReportFailed>();

            if(data?.Request == null)
            {
                Logger.Error("Request parameters are missing from Perfios Report pull retry notification");
            }

            var reportParams = Newtonsoft.Json.JsonConvert.DeserializeObject<PerfiosReportPullRequest>(data.Request.ToString());

            var ongoingRetry = await RetryAuditRespository.GetDuplicateRetryEffortDetails(data.EntityId, data.EntityType, @event.TenantId, reportParams.perfiosTransactionId, reportParams.transactionId);

            if(ongoingRetry != null)
            {
               
                return;
            }

            var PerfiosRetryAuditEntry = new PerfiosRetryTaskAudit() { EntityId = data.EntityId,
                                                                 EntityType = data.EntityType,
                                                                 TenantId = @event.TenantId,
                                                                 PerfiosTransactionId = reportParams.perfiosTransactionId,
                                                                 TransactionId = reportParams.transactionId,
                                                                 threadId = Thread.CurrentThread.ManagedThreadId.ToString()};
            
            var PerfiosRetryTaskConfiguration = configuration.Get();
            int retryCount = 0;

            PerfiosRetryAuditEntry.IsSuccess = false;
            PerfiosRetryAuditEntry.AuditTime = TenantTime.Now;
            PerfiosRetryAuditEntry.FailureMessage = "Starting with Perfios retry, initial log entry to block other threads from entering retry logic";
            PerfiosRetryAuditEntry.AttemptCount = -1;
            RetryAuditRespository.Add(PerfiosRetryAuditEntry);

            while (retryCount < PerfiosRetryTaskConfiguration.PerfiosRetryConfig.MaxAttempts)
            {
                try
                {
                    Logger.Info("Perfios Report Pulled Requested");
                    var PerfiosCreditReportService = PerfiosCreditReportServiceFactory.Create(reader);
                    var tryResponse = await PerfiosCreditReportService.RetrieveXmlReport(data.EntityType, data.EntityId);
                    if(!string.IsNullOrEmpty(tryResponse.ReferenceNumber))
                    {
                        PerfiosRetryAuditEntry.IsSuccess = true;
                        PerfiosRetryAuditEntry.AuditTime = TenantTime.Now;
                        PerfiosRetryAuditEntry.FailureMessage = string.Empty;
                        PerfiosRetryAuditEntry.AttemptCount = retryCount;
                        RetryAuditRespository.Add(PerfiosRetryAuditEntry);
                        break;
                    }
                }catch(Exception ex)
                {
                    if (retryCount == PerfiosRetryTaskConfiguration.PerfiosRetryConfig.MaxAttempts-1)
                    {
                     await  RetryAuditRespository.RemoveAuditEntry(data.EntityId, data.EntityType, @event.TenantId, reportParams.perfiosTransactionId, reportParams.transactionId);
                    }
                    else
                    {
                        Logger.Error("Perfios Report Pull Fail", ex);
                        PerfiosRetryAuditEntry.IsSuccess = false;
                        PerfiosRetryAuditEntry.AuditTime = TenantTime.Now;
                        PerfiosRetryAuditEntry.FailureMessage = ex.Message;
                        PerfiosRetryAuditEntry.AttemptCount = retryCount;
                        RetryAuditRespository.Add(PerfiosRetryAuditEntry);
                    }
                }

                retryCount += 1;
                int nextAttemptDelay = 0;
                if (PerfiosRetryTaskConfiguration.PerfiosRetryConfig.AttemptDelay.Length >= retryCount)
                {
                    nextAttemptDelay = PerfiosRetryTaskConfiguration.PerfiosRetryConfig.AttemptDelay[retryCount - 1];
                }
                else
                {
                    nextAttemptDelay = PerfiosRetryTaskConfiguration.PerfiosRetryConfig.AttemptDelay[PerfiosRetryTaskConfiguration.PerfiosRetryConfig.AttemptDelay.Length - 1];
                }
                Thread.Sleep(nextAttemptDelay);
            }            
        }
    }
}