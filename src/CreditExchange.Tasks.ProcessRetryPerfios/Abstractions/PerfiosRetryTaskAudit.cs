﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Tasks.ProcessRetryPerfios.Abstractions
{
    public class PerfiosRetryTaskAudit : Aggregate, IPerfiosRetryTaskAudit
    {
        public DateTimeOffset AuditTime { get; set; }

        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string FailureMessage { get; set; }

        public string PerfiosTransactionId { get; set; }

        public bool IsSuccess { get; set; }

        public string TransactionId { get; set; }

        public string threadId { get; set; }
        public int AttemptCount { get; set; }
    }
}
