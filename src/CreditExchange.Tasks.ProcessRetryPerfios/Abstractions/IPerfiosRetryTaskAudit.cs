﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Tasks.ProcessRetryPerfios.Abstractions
{
    public interface IPerfiosRetryTaskAudit : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string PerfiosTransactionId { get; set; }
        string TransactionId { get; set; }
        bool IsSuccess { get; set; }
        string FailureMessage { get; set; }
        DateTimeOffset AuditTime { get; set; }
        int AttemptCount { get; set; }
    }
}
