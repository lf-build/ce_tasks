﻿namespace CreditExchange.Tasks.ProcessRetryPerfios.Abstractions
{
    public class PerfiosReportPullRequest
    {
        public string perfiosTransactionId { get; set; }
        public string transactionId { get; set; }
    }
}
