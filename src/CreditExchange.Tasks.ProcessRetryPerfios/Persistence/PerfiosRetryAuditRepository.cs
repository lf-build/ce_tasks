﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using CreditExchange.Tasks.ProcessRetryPerfios.Abstractions;

namespace CreditExchange.Tasks.ProcessRetryPerfios.Persistence
{
    public class PerfiosRetryAuditRepository : MongoRepository<IPerfiosRetryTaskAudit, PerfiosRetryTaskAudit>, IPerfiosRetryAuditRepository
    {
        static PerfiosRetryAuditRepository()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(PerfiosRetryTaskAudit)))
            {
                BsonClassMap.RegisterClassMap<PerfiosRetryTaskAudit>(map =>
                {
                    map.AutoMap();
                    var type = typeof(PerfiosRetryTaskAudit);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
        }
        public PerfiosRetryAuditRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "perfios-retry-audit")
        {
            CreateIndexIfNotExists("entityType", Builders<IPerfiosRetryTaskAudit>.IndexKeys.Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entityId", Builders<IPerfiosRetryTaskAudit>.IndexKeys.Ascending(i => i.EntityId));
          
         
        }

        public Task<IPerfiosRetryTaskAudit> GetDuplicateRetryEffortDetails(string entityId, string entityType, string tenantId, string perfiosTransactionId, string transactionId)
        {
            return Task.FromResult<IPerfiosRetryTaskAudit>
            (
                Query.FirstOrDefault(x => x.EntityId == entityId
                                    && x.EntityType == entityType
                                    && x.TenantId == tenantId
                                    && x.PerfiosTransactionId == perfiosTransactionId
                                    && x.TransactionId == transactionId)
            );
            
        }
        public Task RemoveAuditEntry(string entityId, string entityType,string tenantId, string perfiosTransactionId, string transactionId)
        {

           return Task.Run(()=> Collection.DeleteMany(x => x.EntityId == entityId
                                    && x.EntityType == entityType
                                    && x.TenantId == tenantId
                                    && x.PerfiosTransactionId == perfiosTransactionId
                                    && x.TransactionId == transactionId));

           
        }

        
    }
}
