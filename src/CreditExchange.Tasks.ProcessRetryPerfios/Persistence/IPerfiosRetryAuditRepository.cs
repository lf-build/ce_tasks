﻿using CreditExchange.Tasks.ProcessRetryPerfios.Abstractions;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryPerfios.Persistence
{
    public interface IPerfiosRetryAuditRepository : IRepository<IPerfiosRetryTaskAudit>
    {
        Task<IPerfiosRetryTaskAudit> GetDuplicateRetryEffortDetails(string entityId, string entityType, string tenantId, string perfiosTransactionId, string transactionId);

        Task RemoveAuditEntry(string entityId, string entityType, string tenantId, string perfiosTransactionId, string transactionId);
    }
}