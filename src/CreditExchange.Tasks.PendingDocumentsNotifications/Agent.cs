using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using CreditExchange.Applications.Filters.Client;
using LendFoundry.VerificationEngine.Client;
using CreditExchange.Tasks.Agent;
using System;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.Tasks.PendingDocumentsNotifications.Persistence;
using CreditExchange.Tasks.PendingDocumentsNotifications.Abstractions;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.PendingDocumentsNotifications
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IApplicationFilterClientServiceFactory applicationFilterClientServiceFactory,
            ILoggerFactory loggerFactory,
            IVerificationEngineServiceClientFactory verificationEngineServiceFactory,
            IPendingDocNotificationAudit pendingDocNotificationAudit,
            ITenantServiceFactory tenantServiceFactory,
            IMongoConfiguration mongoConfiguration)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            ApplicationFilterClientServiceFactory = applicationFilterClientServiceFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            VerificationEngineServiceFactory = verificationEngineServiceFactory;
            TenantServiceFactory = tenantServiceFactory;
            MongoConfiguration = mongoConfiguration;
        }

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IApplicationFilterClientServiceFactory ApplicationFilterClientServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private IVerificationEngineServiceClientFactory VerificationEngineServiceFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IMongoConfiguration MongoConfiguration { get; }

        #endregion

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName, DateTime.Now.AddHours(1), "PendingApplicationNotificationProcessor", new string[] { });
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var applicationFilters = ApplicationFilterClientServiceFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var VerificationEngineService = VerificationEngineServiceFactory.Create(reader);
            var tenantService = TenantServiceFactory.Create(reader);
            var PendingDocNotificationAudit = new PendingDocNotificationAudit(tenantService, MongoConfiguration);

            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Configuration was not found for pending documents notifications");
                return;
            }

            var pendingDocumentNotificationsConfiguration = configuration.Get();
            if (pendingDocumentNotificationsConfiguration == null)
            {
                logger.Error("Was not found the Configuration related to InActive application notifications");
                return;
            }

            try
            {
                var applications = await applicationFilters.GetTagInfoForApplicationsByTag(pendingDocumentNotificationsConfiguration.PendingDocumentApplications.PendingDocumentTagName);
                var appToBeProcessed = applications?.TakeWhile(x => tenantTime.Now.Subtract(x.Tags
                                                                                            .FirstOrDefault(y => y.TagName == pendingDocumentNotificationsConfiguration.PendingDocumentApplications.PendingDocumentTagName).TaggedOn).TotalDays > pendingDocumentNotificationsConfiguration.PendingDocumentApplications.PendingDocNotificationThreshold);
                var appsToBeProcessedList = appToBeProcessed.Select(x => x.ApplicationNumber);
                var applicationsToBeProcessed = applicationFilters.GetAll().Where(x => appsToBeProcessedList.Contains(x.ApplicationNumber) && !pendingDocumentNotificationsConfiguration.PendingDocumentApplications.ExcludedStatuses.Contains(x.StatusCode));

                object sync = new object();

                foreach (var application in applicationsToBeProcessed)
                {
                    try
                    {
                        var pendingDocuments = await VerificationEngineService.GetPendingDocument("application", application.ApplicationNumber, application.StatusWorkFlowId);
                        var pendingDocNotificationCount = PendingDocNotificationAudit.GetNotificationCount(application.ApplicationNumber);

                        if (pendingDocuments != null && pendingDocuments.Count() > 0)
                        {
                            pendingDocuments = pendingDocuments.Where(x => x.DocumentName.Count() > 0 && pendingDocumentNotificationsConfiguration.PendingDocumentApplications.IncludedDocuments.Contains(x.MethodName)).ToList();
                        }

                        lock (sync)
                        {
                            if (pendingDocuments != null && pendingDocuments.Count() > 0 && pendingDocNotificationCount < pendingDocumentNotificationsConfiguration.PendingDocumentApplications.MaxAttempts)
                            {
                                eventHub.Publish("NotifyPendingDocuments", new { PendingDocApplication = application, PendingDocuments = pendingDocuments });
                                PendingDocNotificationAudit.Add(new PendingDocNotificationEntry() { ApplicationNumber = application.ApplicationNumber, SentOn = new TimeBucket(tenantTime.Now), TenantId = Tenant });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Pending notification processing failed for application #{application.ApplicationNumber}", ex);
                    }
                }

                //Parallel.ForEach(applicationsToBeProcessed, async application =>
                //{
                //    try
                //    {
                //        var pendingDocuments = await VerificationEngineService.GetPendingDocument("application", application.ApplicationNumber);
                //        var pendingDocNotificationCount = PendingDocNotificationAudit.GetNotificationCount(application.ApplicationNumber);

                //        if (pendingDocuments != null && pendingDocuments.Count() > 0)
                //        {
                //            pendingDocuments = pendingDocuments.Where(x => x.DocumentName.Count() > 0 && pendingDocumentNotificationsConfiguration.PendingDocumentApplications.IncludedDocuments.Contains(x.MethodName)).ToList();
                //        }

                //        lock (sync)
                //        {
                //            if (pendingDocuments != null && pendingDocuments.Count() > 0 && pendingDocNotificationCount < pendingDocumentNotificationsConfiguration.PendingDocumentApplications.MaxAttempts)
                //            {
                //                eventHub.Publish("NotifyPendingDocuments", new { PendingDocApplication = application, PendingDocuments = pendingDocuments });
                //                PendingDocNotificationAudit.Add(new PendingDocNotificationEntry() { ApplicationNumber = application.ApplicationNumber, SentOn = new TimeBucket(tenantTime.Now), TenantId = Tenant });
                //            }
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        logger.Error($"Pending notification processing failed for application #{application.ApplicationNumber}", ex);
                //    }
                //});

                logger.Info("End pending document notifications processing");
            }
            catch (Exception ex)
            {
                logger.Error($"Failed attempt to process pending documents notifications.", ex);
            }
        }
    }
}