﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.PendingDocumentsNotifications
{
    public class PendingDocumentNotification
    {
        public int PendingDocNotificationThreshold { get; set; } 
        public string PendingDocumentTagName { get; set; }
        public List<string> ExcludedStatuses { get; set; }
        public List<string> IncludedDocuments { get; set; }
        public int MaxAttempts { get; set; }
    }
}