﻿using CreditExchange.Tasks.PendingDocumentsNotifications.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Linq;

namespace CreditExchange.Tasks.PendingDocumentsNotifications.Persistence
{
    public class PendingDocNotificationAudit : MongoRepository<IPendingDocNotificationEntry, PendingDocNotificationEntry>, IPendingDocNotificationAudit
    {       
        public PendingDocNotificationAudit(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "PendingDocNotifications")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(PendingDocNotificationEntry)))
            {
                BsonClassMap.RegisterClassMap<PendingDocNotificationEntry>(map =>
                {
                    map.AutoMap();
                    var type = typeof(PendingDocNotificationEntry);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            CreateIndexIfNotExists("ApplicationNumber", Builders<IPendingDocNotificationEntry>.IndexKeys.Ascending(i => i.ApplicationNumber));            
        }

        public int GetNotificationCount(string applicationNumber)
        {
            var result = Query.Where(x => x.ApplicationNumber == applicationNumber && x.TenantId == TenantService.Current.Id).ToList();
            return result != null ? result.Count() : 0;
        }
    }
}
