﻿using CreditExchange.Tasks.PendingDocumentsNotifications.Abstractions;
using LendFoundry.Foundation.Persistence;

namespace CreditExchange.Tasks.PendingDocumentsNotifications.Persistence
{
    public interface IPendingDocNotificationAudit : IRepository<IPendingDocNotificationEntry>
    {
        int GetNotificationCount(string applicationNumber);
    }
}
