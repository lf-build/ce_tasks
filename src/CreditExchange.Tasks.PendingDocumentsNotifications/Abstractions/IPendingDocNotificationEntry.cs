﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.PendingDocumentsNotifications.Abstractions
{
    public interface IPendingDocNotificationEntry : IAggregate
    {
        string ApplicationNumber { get; set; }
        TimeBucket SentOn { get; set; }
    }
}
