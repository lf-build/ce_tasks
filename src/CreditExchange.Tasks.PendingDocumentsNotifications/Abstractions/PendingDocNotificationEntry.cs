﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Tasks.PendingDocumentsNotifications.Abstractions
{
    public class PendingDocNotificationEntry : Aggregate, IPendingDocNotificationEntry
    {
        public string ApplicationNumber { get; set; }

        public TimeBucket SentOn { get; set; }
    }
}
