using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.PendingDocumentsNotifications
{
    public static class Settings
    {
        public static string ServiceName { get; } = "pending-documents-notifications";

        public static string Prefix = "PENDING-DOCUMENTS-NOTIFICATIONS";
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings ApplicationFilter { get; } = new ServiceSettings($"{Prefix}_APPLICATIONFILTER_HOST", "application-filters", $"{Prefix}_APPLICATIONFILTER_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings VerificationEngine { get; } = new ServiceSettings($"{Prefix}_VERIFICATION_ENGINE", "verification-engine");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT_SERVICE_HOST", "tenant", $"{Prefix}_TENANT_SERVICE_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}