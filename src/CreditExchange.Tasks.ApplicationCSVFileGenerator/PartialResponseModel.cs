﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ApplicationCSVFileGenerator
{
    public class PartialResponseModel
    {

        public string title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string marital_status { get; set; }
        public string mobile { get; set; }
        public bool IsMobileVerified { get; set; }
        public string mobileotpreference { get; set; }
        public string email { get; set; }

    }

    public class FullResponseModel
    {
        public ObjectId Id { get; set; }
        public string OtherPurposeDescription { get; set; }
        public string purposeOfLoan { get; set; }
        public double requestedAmount { get; set; }
        public int requestedTermType { get; set; }
        public string aadhaarNumber { get; set; }
        public DateTimeOffset? dateOfBirth { get; set; }
        public string salutation { get; set; }
        public string gender { get; set; }
        public string permanentAccountNumber { get; set; }
        public string residenceType { get; set; }
        public string currentAddressLine1 { get; set; }
        public string currentAddressLine2 { get; set; }
        public string currentAddressLine3 { get; set; }
        public string currentAddressLine4 { get; set; }
        public string currentCity { get; set; }
        public string currentCountry { get; set; }
        public string currentLandMark { get; set; }
        public string currentLocation { get; set; }
        public string currentPinCode { get; set; }
        public string currentState { get; set; }
        public string permanentAddressLine1 { get; set; }
        public string permanentAddressLine2 { get; set; }
        public string permanentAddressLine3 { get; set; }
        public string permanentAddressLine4 { get; set; }
        public string permanentCity { get; set; }
        public string permanentCountry { get; set; }
        public string permanentLandMark { get; set; }
        public string permanentLocation { get; set; }
        public string permanentPinCode { get; set; }
        public string permanentState { get; set; }
        public DateTimeOffset? employmentAsOfDate { get; set; }
        public string employmentStatus { get; set; }
        public string employerName { get; set; }
        public string workEmail { get; set; }
        public double? income { get; set; }
        public double? creditCardBalances { get; set; }
        public string paymentFrequency { get; set; }
        public double? debtPayments { get; set; }
        public double? monthlyExpenses { get; set; }
        public double? monthlyRent { get; set; }
        public string sourceReferenceId { get; set; }
        public string sourceType { get; set; }
        public string mobile { get; set; }
        public string systemChannel { get; set; }
        public string trackingCode { get; set; }
        public DateTime insertedOn { get; set; }

        public string title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string marital_status { get; set; }
        public string personalMobile { get; set; }
        public bool isMobileVerified { get; set; }
        public string mobileVerificationTime { get; set; }
        public string mobileVerificationNotes { get; set; }
        public string personalEmail { get; set; }
    }


    public class PartialDump
    {
        public ObjectId Id { get; set; }
        public double Amount { get; set; }
        public ReasonObj Reason { get; set; }
         public ApplicantData You { get; set; }
    }

    public class ReasonObj
    {
        public string Reason { get; set; }
        public string Description { get; set; }
    }


    public class ApplicantData
    {

        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Marital_status { get; set; }
        public MobileObj Mobile { get; set; }
        public int Count { get; set; }
        [DataMember(Name = "mobile-otp-ref")]
        public string Mobileotpref { get; set; }
        public string email { get; set; }

    }
    public class MobileObj
    {
        public string Number { get; set; }
        public bool Verified { get; set; }
    }
}