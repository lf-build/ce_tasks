﻿using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Tasks.ApplicationCSVFileGenerator
{
    public class Application
    {
        public ObjectId Id { get; set; }

        [JsonIgnore]
        public string TenantId { get; set; }
        public double RequestedAmount { get; set; }
        public int RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }
        //TODO: What is other purpose of loan?
        public string OtherPurposeDescription { get; set; }
        public Source Source { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public TimeBucket ExpiredDate { get; set; }
        public string ApplicationNumber { get; set; }
        public TimeBucket ApplicationDate { get; set; }
        public EmploymentDetail EmploymentDetail { get; set; }
        public string ResidenceType { get; set; }

        public SelfDeclareExpense SelfDeclareExpense { get; set; }



        public BankInformation BankInformation { get; set; }

        public EmailAddress EmailAddress { get; set; }

        public IncomeInformation IncomeInformation { get; set; }

        public string ApplicantId { get; set; }


        public List<Address> Addresses { get; set; }


        public List<PhoneNumber> PhoneNumbers { get; set; }
    }


    public class Address
    {
        public string AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string LandMark { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public bool IsDefault { get; set; }
    }

    public class BankInformation
    {
        public string BankId { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountHolderName { get; set; }
        public string IfscCode { get; set; }
        public string AccountType { get; set; }


        public Address BankAddresses { get; set; }
    }

    public class EducationInformation
    {
        public string LevelOfEducation { get; set; }
        public string EducationalInstitution { get; set; }
    }

    public class EmailAddress
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string EmailType { get; set; }
        public bool IsDefault { get; set; }
    }
    public class EmploymentDetail
    {
        public string EmploymentId { get; set; }


        public List<Address> Addresses { get; set; }

        public string Designation { get; set; }

        public int LengthOfEmploymentInMonths { get; set; }

        public string Name { get; set; }

        public string WorkEmail { get; set; }


        public List<PhoneNumber> WorkPhoneNumbers { get; set; }


        public IncomeInformation IncomeInformation { get; set; }

        public TimeBucket AsOfDate { get; set; }

        public string EmploymentStatus { get; set; }

        public string CinNumber { get; set; }

    }

    public class IncomeInformation
    {
        public double Income { get; set; }
        public string PaymentFrequency { get; set; }
    }

    public class PhoneNumber
    {
        public string PhoneId { get; set; }
        public string Phone { get; set; }
        public string CountryCode { get; set; }
        public string PhoneType { get; set; }
        public bool IsDefault { get; set; }
    }
    public class SelfDeclareExpense
    {

        public double MonthlyRent { get; set; }
        public double MonthlyExpenses { get; set; }
        public double CreditCardBalances { get; set; }
        public double DebtPayments { get; set; }
    }

    public class Source
    {
        public string TrackingCode { get; set; }

        public string SystemChannel { get; set; }

        public string SourceType { get; set; }

        public string SourceReferenceId { get; set; }
    }
}
