﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using MongoDB.Bson;

namespace CreditExchange.Tasks.ApplicationCSVFileGenerator
{
    public class Applicant 
    {
        public ObjectId Id { get; set; }

        [JsonIgnore]
        public string TenantId { get; set; }
        public string UserId { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public DateTimeOffset? DateOfBirth { get; set; }

       
        public List<EmailAddress> EmailAddresses { get; set; }

     
        public List<PhoneNumber> PhoneNumbers { get; set; }

     
        public List<Address> Addresses { get; set; }

        public string AadhaarNumber { get; set; }
        public string PermanentAccountNumber { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }

      
        public List<IncomeInformation> IncomeInformation { get; set; }

     
        public List<EmploymentDetail> EmploymentDetails { get; set; }

       
        public List<BankInformation> BankInformation { get; set; }

       
        public EducationInformation HighestEducationInformation { get; set; }
        public DateTime? CanReapplyBy { get; set; }
        
    }
}
