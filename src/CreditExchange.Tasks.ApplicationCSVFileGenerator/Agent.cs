
using LendFoundry.Email;
using LendFoundry.Email.Client;
using CreditExchange.Tasks.Agent;
using CsvHelper;
using CsvHelper.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using LendFoundry.Configuration;


namespace CreditExchange.Tasks.ApplicationCSVFileGenerator
{
    public class Agent : ScheduledAgent
    {
       

        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IMongoConfiguration mongoConfiguration,
            //IEmailServiceFactory emailFactory,
            ILoggerFactory loggerFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            MongoConfiguration = mongoConfiguration;
            TenantTimeFactory = tenantTimeFactory;
            //EmailFactory = emailFactory;
            LoggerFactory = loggerFactory;
        }

        private IMongoConfiguration MongoConfiguration { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        //private IEmailServiceFactory EmailFactory { get; }
        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }


        public byte[] WriteToCsv<T>(IEnumerable<T> items)
        {
            try
            {

                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream))
                    using (var csvWriter = new CsvWriter(streamWriter))
                    {
                        csvWriter.WriteRecords(items);
                    }
                    return memoryStream.ToArray();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        //public void WriteCSV<T>(IEnumerable<T> items, string path, string fileName)
        //{
        //    Type itemType = typeof(T);
        //    var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
        //                        .OrderBy(p => p.Name);


        //    using (var memoryStream = new MemoryStream())
        //    {

        //        using (var writer = new StreamWriter(memoryStream))
        //        {

        //            writer.WriteLine(string.Join(", ", props.Select(p => p.Name)));

        //            foreach (var item in items)
        //            {
        //                writer.WriteLine(string.Join(", ", props.Select(p => p.GetValue(item, null))));
        //            }
        //        }

        //        byte[] bytes = memoryStream.ToArray();

        //        using (var csvMemoryStream = new MemoryStream(bytes))
        //        {
        //                        FileStorageService.Upload(csvMemoryStream, fileName, path);

        //        }
        //    }

        //}

        public static void SendMail(List<string> recipientList,Configuration configuration, List<string> attachmentFilename, List<Stream> streams)
        {
            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredential = new NetworkCredential(configuration.UserName,configuration.Password);
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress(configuration.FromEmailAddress);//"anuj@qbera.com"

            // setup up the host, increase the timeout to 5 minutes
            smtpClient.Host = configuration.SmtpHost; // "email-smtp.us-east-1.amazonaws.com";
            smtpClient.Port = configuration.SmtpPort;
            smtpClient.EnableSsl = configuration.EnableSsl; //  true;
            smtpClient.UseDefaultCredentials = configuration.UseDefaultCredentials;// false;
            smtpClient.Credentials = basicCredential;
            smtpClient.Timeout = configuration.SmptTimeout;// (60 * 5 * 1000);

            message.From = fromAddress;
            message.Subject = configuration.EmailSubject;
            message.IsBodyHtml = false;
            message.Body = configuration.EmailBody;

            foreach (var recipient in recipientList)
            {
                message.To.Add(recipient);
            }         

            if (attachmentFilename != null)
            {
                for (int i = 0; i < streams.Count(); i++)
                {
                    message.Attachments.Add(new Attachment(streams[i], attachmentFilename[i]));
                }
            }

            smtpClient.Send(message);
        }



        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName, DateTime.Now.AddHours(1), "AutoExpiryTask", new string[] { });
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            //var email = EmailFactory.Create(reader);

            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to application filter expiry status Task");
                return;
            }

            var applicationConfiguration = configuration.Get();
            if (applicationConfiguration == null)
            {
                logger.Error("Was not found the Configuration related to application csv generator");
                return;
            }

            if (string.IsNullOrWhiteSpace(applicationConfiguration.MongoConnectionString))
            {
                logger.Error("Was not found the Configuration related to Mongo Connection String");
                return;
            }
            if (string.IsNullOrWhiteSpace(applicationConfiguration.SmtpHost))
            {
                logger.Error("Was not found the Configuration related to SMTP Host");
                return;
            }
            if (string.IsNullOrWhiteSpace(applicationConfiguration.UserName))
            {
                logger.Error("Was not found the Configuration related to SMTP UserName");
                return;
            }
            if (string.IsNullOrWhiteSpace(applicationConfiguration.FromEmailAddress))
            {
                logger.Error("Was not found the Configuration related to From Email Address");
                return;
            }
            if (string.IsNullOrWhiteSpace(applicationConfiguration.ToEmailAddress))
            {
                logger.Error("Was not found the Configuration related to To Email Address");
                return;
            }
            try
            {

                var dateProcessed = string.IsNullOrWhiteSpace(applicationConfiguration.ApplicationDate) ? DateTime.Now : DateTime.ParseExact(applicationConfiguration.ApplicationDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                var client = new MongoClient(applicationConfiguration.MongoConnectionString.ToString());
                var server = client.GetServer();
                MongoDatabase _database = server.GetDatabase("applications");
                var _collection = _database.GetCollection<Application>("application");

                var applicationData = _collection.FindAll().Where(x => x.ApplicationDate.Time.Date == dateProcessed.Date).ToList();

                MongoDatabase _databaseApplicant = server.GetDatabase("applicants");
                var _applicant = _databaseApplicant.GetCollection<Applicant>("applicants");

                MongoDatabase _databaseDump = server.GetDatabase("data-dump"); //need to change
                var _partial = _databaseDump.GetCollection<PartialDump>("partial");

                List<PartialResponseModel> partialModel = new List<PartialResponseModel>();
                List<FullResponseModel> fullModel = new List<FullResponseModel>();

                foreach (var applicationObj in applicationData)
                {
                    var query = Query<Applicant>.EQ(x => x.Id, ObjectId.Parse(applicationObj.ApplicantId));
                    var result = _applicant.FindOne(query);
                    //var result = _applicant.Find(x => x.Id == ObjectId.Parse(applicationObj.ApplicantId)).FirstOrDefault();
                    PartialDump resultFull = null;
                    try
                    {
                        var queryFull = Query<PartialDump>.EQ(x => x.You.email, result.EmailAddresses?.FirstOrDefault().Email);
                        resultFull = _partial.FindOne(queryFull);
                        //if (result.EmailAddresses != null)
                        //    resultFull = _partial.Find(x => x.You.email == result.EmailAddresses.FirstOrDefault().Email).FirstOrDefault();

                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Failed get application data.",ex);
                    }

                    PartialResponseModel partial = new PartialResponseModel();
                    partial.email = result.EmailAddresses?.FirstOrDefault().Email;
                    partial.FirstName = result.FirstName;
                    partial.MiddleName = result.MiddleName;
                    partial.LastName = result.LastName;
                    partial.mobile = result.PhoneNumbers?.FirstOrDefault().Phone;
                    partial.title = result.Salutation;
                    partial.marital_status = result.MaritalStatus;
                    partial.mobileotpreference = resultFull != null ? resultFull.You?.Mobileotpref : null;
                    partial.IsMobileVerified = resultFull != null ? resultFull.You?.Mobile?.Verified ?? true : false;
                    partialModel.Add(partial);

                    FullResponseModel full = new FullResponseModel();
                    full.Id = applicationObj.Id;
                    full.OtherPurposeDescription = applicationObj.OtherPurposeDescription;
                    full.purposeOfLoan = applicationObj.PurposeOfLoan;
                    full.requestedAmount = applicationObj.RequestedAmount;
                    full.requestedTermType = applicationObj.RequestedTermType;
                    full.aadhaarNumber = result.AadhaarNumber;
                    full.dateOfBirth = result.DateOfBirth;
                    full.gender = result.Gender;
                    full.permanentAccountNumber = result.PermanentAccountNumber;
                    full.residenceType = applicationObj.ResidenceType;
                    full.mobile = result.PhoneNumbers?.FirstOrDefault().Phone;

                    var currentAddress = applicationObj.Addresses?.FirstOrDefault(x => x.AddressType == "Current");
                    full.currentAddressLine1 = currentAddress?.AddressLine1?.Replace(",", " ");
                    full.currentAddressLine2 = currentAddress?.AddressLine2?.Replace(",", " ");
                    full.currentAddressLine3 = currentAddress?.AddressLine3?.Replace(",", " ");
                    full.currentAddressLine4 = currentAddress?.AddressLine4?.Replace(",", " ");
                    full.currentCity = currentAddress?.City;
                    full.currentCountry = currentAddress?.Country;
                    full.currentLandMark = currentAddress?.LandMark?.Replace(",", " ");
                    full.currentLocation = currentAddress?.Location;
                    full.currentPinCode = currentAddress?.PinCode;
                    full.currentState = currentAddress?.State;

                    var permanentAddress = applicationObj.Addresses?.FirstOrDefault(x => x.AddressType == "Permanant");
                    full.permanentAddressLine1 = permanentAddress?.AddressLine1?.Replace(",", " ");
                    full.permanentAddressLine2 = permanentAddress?.AddressLine2?.Replace(",", " ");
                    full.permanentAddressLine3 = permanentAddress?.AddressLine3?.Replace(",", " ");
                    full.permanentAddressLine4 = permanentAddress?.AddressLine4?.Replace(",", " ");
                    full.permanentCity = permanentAddress?.City;
                    full.permanentCountry = permanentAddress?.Country;
                    full.permanentLandMark = permanentAddress?.LandMark?.Replace(",", " ");
                    full.permanentLocation = permanentAddress?.Location;
                    full.permanentPinCode = permanentAddress?.PinCode;
                    full.permanentState = permanentAddress?.State;

                    full.employmentAsOfDate = applicationObj.EmploymentDetail?.AsOfDate?.Time;
                    full.employmentStatus = applicationObj.EmploymentDetail?.EmploymentStatus;
                    full.employerName = applicationObj.EmploymentDetail?.Name;
                    full.workEmail = applicationObj.EmploymentDetail?.WorkEmail;
                    full.income = applicationObj.EmploymentDetail?.IncomeInformation?.Income;
                    full.creditCardBalances = applicationObj.SelfDeclareExpense?.CreditCardBalances;
                    full.paymentFrequency = applicationObj.IncomeInformation?.PaymentFrequency;
                    full.debtPayments = applicationObj.SelfDeclareExpense?.DebtPayments;
                    full.monthlyExpenses = applicationObj.SelfDeclareExpense?.MonthlyExpenses;
                    full.monthlyRent = applicationObj.SelfDeclareExpense?.MonthlyRent;
                    full.sourceReferenceId = applicationObj.Source?.SourceReferenceId;
                    full.sourceType = applicationObj.Source?.SourceType;
                    full.systemChannel = applicationObj.Source?.SystemChannel;
                    full.trackingCode = applicationObj.Source?.TrackingCode;
                    full.insertedOn = DateTime.Now;
                    full.isMobileVerified = resultFull != null ? resultFull.You?.Mobile?.Verified ?? true : false; ;
                    full.mobileVerificationNotes = "Testing";
                    full.mobileVerificationTime = "11/5/1981";
                    var PersonalEmail = result.EmailAddresses.FirstOrDefault(x => x.EmailType == "Personal");
                    full.personalEmail = PersonalEmail?.Email;
                    full.FirstName = result.FirstName;
                    full.MiddleName = result.MiddleName;
                    full.LastName = result.LastName;
                    full.marital_status = result.MaritalStatus;
                    var personalMobile = result.PhoneNumbers.FirstOrDefault(x => x.PhoneType == "Personal");
                    full.personalMobile = personalMobile?.Phone;
                    full.title = result.Salutation;
                    fullModel.Add(full);
                }

                var fileNamePartial = "Partial_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                //var partialPath = applicationConfiguration.SavePath + "/" + fileNamePartial;

                var fileNameFull = "Full_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                //var fullPath = applicationConfiguration.SavePath + "/" + fileNameFull;


                //     WriteCSV(partialModel, @applicationConfiguration.SavePath, fileNamePartial);
                //    WriteCSV(fullModel, @applicationConfiguration.SavePath, fileNameFull);
                List<Stream> stream = new List<Stream>();
                var partialByte = WriteToCsv(partialModel);
                Stream partialStream = new MemoryStream(partialByte);
                stream.Add(partialStream);

                var fullByte = WriteToCsv(fullModel);
                Stream fullStream = new MemoryStream(fullByte);
                stream.Add(fullStream);
                List<string> filepath = new List<string>();
                filepath.Add(fileNamePartial);
                filepath.Add(fileNameFull);

                try
                {
                    List<string> recipientList = applicationConfiguration.ToEmailAddress.Split(',').ToList();
                
                    SendMail(recipientList, applicationConfiguration, filepath, stream);
                } 
                catch (Exception ex)
                {
                    Console.WriteLine("Exception while sending mail: " + ex.Message);
                }


            }
            catch (Exception ex)
            {
                logger.Error($"Exception while sending mail.", ex);
            }
        }
    }
}
