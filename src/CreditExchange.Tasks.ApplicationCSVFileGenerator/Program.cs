﻿
using LendFoundry.Email.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;

namespace CreditExchange.Tasks.ApplicationCSVFileGenerator
{
    public class Program : DependencyInjection
    {
        public void Main(string[] args)
        {
            Provider.GetService<IAgent>().Execute();
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));            
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            //services.AddEmailService(Settings.Email.Host, Settings.Email.Port);
            services.AddTransient<IAgent, Agent>();
            //    services.AddTransient<IFileStorageService, SftpService>();
            //    services.AddTransient<ISftpProviderConfiguration>(provider =>
            //    {
            //        var TokenHandler = provider.GetService<ITokenHandler>();
            //        var token = TokenHandler.Issue(Settings.Tenant, Settings.ServiceName);
            //        var reader = new StaticTokenReader(token.Value);
            //        var factory = provider.GetService<IConfigurationServiceFactory<Configuration>>();
            //        var configurationService = factory.Create(reader);
            //        var result = configurationService.Get();
            //        var sftpConfiguration = new SftpProviderConfiguration
            //        {
            //            Host = result.SftpConfiguration.Host,
            //            Port = result.SftpConfiguration.Port,
            //            Username = result.SftpConfiguration.Username,
            //            Password = result.SftpConfiguration.Password,
            //        };
            //        return sftpConfiguration;
            //    });
            return services;
        }
    }
}
