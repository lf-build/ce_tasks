

namespace CreditExchange.Tasks.ApplicationCSVFileGenerator
{
    public class Configuration
    {
        public string ApplicationDate { get; set; }
        public string SavePath { get; set; }
        public string LogPath { get; set; }
        public string MongoConnectionString { get; set; }
        public string FromEmailAddress { get; set; }

        public string ToEmailAddress { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public bool EnableSsl { get; set; }

        public bool UseDefaultCredentials { get; set; }

        public int SmptTimeout { get; set; }

        public string EmailSubject { get; set; }

        public string EmailBody { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }

    }
}