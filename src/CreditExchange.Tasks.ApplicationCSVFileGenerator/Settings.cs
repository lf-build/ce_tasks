using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.ApplicationCSVFileGenerator
{
    public static class Settings
    {
        public static string ServiceName { get; } = "applicationcsvgenerator";
        public static string Prefix = "APPLICATIONCSVGENERATOR";
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        //public static ServiceSettings Email { get; } = new ServiceSettings($"{Prefix}_EMAIL_HOST", "email", $"{Prefix}_EMAIL_PORT");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        public static string Tenant => Environment.GetEnvironmentVariable("TASK_TENANT") ?? "my-tenant";


    }
}