﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using CreditExchange.Tasks.Dispatcher.Configuration;
using CreditExchange.Tasks.Docker.Client;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace CreditExchange.Tasks.Dispatcher
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddSingleton<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddDockerRunner(Settings.Docker.Host, Settings.Docker.Port);
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddConfigurationService<TaskConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddSingleton<ITaskDispatcher, TaskDispatcher>();
            services.AddSingleton(p => p);
            return services;
        }
    }
}