﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.Dispatcher.Configuration
{
    public class Task : ITask
    {
        public string Tenant { get; set; }

        public string Name { get; set; }

        public TaskType Type { get; set; }

        public string Image { get; set; }

        public Dictionary<string, string> Environment { get; set; } = new Dictionary<string, string>();

        public Dictionary<string, string> Links { get; set; } = new Dictionary<string, string>();

        public string Expression { get; set; }
    }
}