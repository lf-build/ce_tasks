namespace CreditExchange.Tasks.Dispatcher.Configuration
{
    public enum TaskType
    {
        Scheduled,
        EventBased
    }
}