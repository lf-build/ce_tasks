﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.Dispatcher.Configuration
{
    public class TaskConfiguration : ITaskConfiguration
    {
        public TaskConfiguration()
        {
            Definitions = new List<Task>();
        }

        public List<Task> Definitions { get; set; }
    }
}
