﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.Dispatcher.Configuration
{
    public interface ITaskConfiguration
    {
        List<Task> Definitions { get; set; }
    }
}