﻿namespace CreditExchange.Tasks.Dispatcher
{
    public interface ITaskDispatcher
    {
        void Start();
    }
}