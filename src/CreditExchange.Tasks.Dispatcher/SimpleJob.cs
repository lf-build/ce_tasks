using CreditExchange.Tasks.Agent;
using Quartz;

namespace CreditExchange.Tasks.Dispatcher
{
    public class SimpleJob : IJob
    {
#if DOTNET2
        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            var runner = context.MergedJobDataMap.Get("runner") as ITaskRunner;
            var task = context.MergedJobDataMap.Get("task") as ITask;
            if (task == null)
                return null;

            //if (task.Environment.ContainsKey(ScheduledAgent.TenantEnvironmentVariable))
            //    task.Environment.Remove(ScheduledAgent.TenantEnvironmentVariable);
            //task.Environment.Add(ScheduledAgent.TenantEnvironmentVariable, task.Tenant);
            return runner?.Run(task);
        }
#else
        public void Execute(IJobExecutionContext context)
        {
            var runner = context.MergedJobDataMap.Get("runner") as ITaskRunner;
            var task = context.MergedJobDataMap.Get("task") as ITask;
            if (task == null)
                return;

            //if (task.Environment.ContainsKey(ScheduledAgent.TenantEnvironmentVariable))
            //    task.Environment.Remove(ScheduledAgent.TenantEnvironmentVariable);
            //task.Environment.Add(ScheduledAgent.TenantEnvironmentVariable, task.Tenant);
            runner?.Run(task);
        }
#endif
    }
}