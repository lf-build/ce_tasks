﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Tasks.Dispatcher
{
    public class EmptyTokenReader : ITokenReader
    {
        public string Read()
        {
            return string.Empty;
        }
    }
}