﻿using System;
using CreditExchange.Tasks.Agent;
using Newtonsoft.Json;
using Quartz;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.EventHub;
using System.Threading.Tasks;
#else
using Microsoft.Framework.DependencyInjection;
using LendFoundry.EventHub.Client;
#endif

namespace CreditExchange.Tasks.Dispatcher
{
    public class EventBasedJob : IJob
    {
#if DOTNET2
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                var task = context.MergedJobDataMap.Get("task") as ITask;

                if (task == null)
                    return;

                var provider = context.MergedJobDataMap.Get("p") as IServiceProvider;

                if (provider == null)
                    return;

                var hub = provider.GetService<IEventHubClient>();

                var runner = context.MergedJobDataMap.Get("runner") as ITaskRunner;
                //hub.On(task.Expression, @event =>
                //{
                //    Console.WriteLine("Hit");
                //    if (@event.TenantId == task.Tenant)
                //    {
                //        //if (task.Environment.ContainsKey(EventBasedAgent.EventNameEnvironmentVariable))
                //        //    task.Environment.Remove(EventBasedAgent.EventNameEnvironmentVariable);
                //        //task.Environment.Add(EventBasedAgent.EventNameEnvironmentVariable, JsonConvert.SerializeObject(@event));
                //        //runner?.Run(task);
                //    }
                //});
                hub.Start();
            });

        }
#else
        public void Execute(IJobExecutionContext context)
        {
            var task = context.MergedJobDataMap.Get("task") as ITask;

            if (task == null)
                return;

            var provider = context.MergedJobDataMap.Get("p") as IServiceProvider;

            if (provider == null)
                return;

            var hub = provider.GetService<IEventHubClient>();

            var runner = context.MergedJobDataMap.Get("runner") as ITaskRunner;
            //hub.On(task.Expression, @event =>
            //{
            //    Console.WriteLine("Hit");
            //    if (@event.TenantId == task.Tenant)
            //    {
            //        //if (task.Environment.ContainsKey(EventBasedAgent.EventNameEnvironmentVariable))
            //        //    task.Environment.Remove(EventBasedAgent.EventNameEnvironmentVariable);
            //        //task.Environment.Add(EventBasedAgent.EventNameEnvironmentVariable, JsonConvert.SerializeObject(@event));
            //        //runner?.Run(task);
            //    }
            //});
            hub.Start();

        }
#endif
    }
}