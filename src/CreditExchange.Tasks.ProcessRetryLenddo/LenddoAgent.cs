﻿using System;
using CreditExchange.Lenddo.Client;
using CreditExchange.Syndication.Lenddo.Events;
using CreditExchange.Tasks.Agent;
using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using CreditExchange.Tasks.ProcessRetryLenddo.Persistence;
using Newtonsoft.Json;
using CreditExchange.Tasks.ProcessRetryLenddo.Abstractions;
using System.Threading;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ProcessRetryLenddo
{
    public class LenddoAgent : EventBasedAgent
    {
        public LenddoAgent(
          IEventHubClientFactory eventHubFactory,
           IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
             ITokenReaderFactory tokenHandlerFactory,
            ITenantTimeFactory tenantTimeFactory,
            ILenddoServiceClientFactory lenddoServiceClientFactory,
            ITenantServiceFactory tenantServiceFactory,
            IMongoConfiguration mongoConfiguration,
            ILogger logger
            )
            : base(eventHubFactory, nameof(LenddoSocialScoreFail), tokenHandler, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            LenddoServiceClientFactory = lenddoServiceClientFactory;
            TenantServiceFactory = tenantServiceFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            MongoConfiguration = mongoConfiguration;
            Logger = logger;
        }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ILenddoServiceClientFactory LenddoServiceClientFactory { get; }
        private ILogger Logger { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IMongoConfiguration MongoConfiguration { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }
        public override void Execute(EventInfo @event)
        {
            var reader = TokenHandlerFactory.Create(@event.TenantId, string.Empty);
            var configuration = ConfigurationFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
            var TenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var tenantService = TenantServiceFactory.Create(reader);
            var RetryAuditRespository = new LenddoRetryAuditRepository(tenantService, MongoConfiguration);

            if (configuration == null)
            {
                Logger.Error("Was not found the Configuration related to Lenddo retry Task");
                return;
            }
            var data = @event.Cast<LenddoSocialScoreFail>();

            if (data?.Request == null)
            {
                Logger.Error("Request parameters are missing from Lenddo Report pull retry notification");
            }

           /// var reportParams = JsonConvert.DeserializeObject<LenddoSocialScoreFail>(data.Request.ToString());

            var ongoingRetry = RetryAuditRespository.GetDuplicateRetryEffortDetails(data.EntityId, data.EntityType, @event.TenantId, data.Request.ToString());

            if (ongoingRetry.Result != null)
            {
                return; // exit as retry from another thread is already in progress.
            }

            var LenddoRetryAuditEntry = new LenddoRetryTaskAudit()
            {
                EntityId = data.EntityId,
                EntityType = data.EntityType,
                TenantId = @event.TenantId,
                ClientId = data.Request.ToString()
            };

            var lenddoRetryTaskConfiguration = configuration.Get();
            int retryCount = 0;
            LenddoRetryAuditEntry.IsSuccess = false;
            LenddoRetryAuditEntry.AuditTime = TenantTime.Now;
            LenddoRetryAuditEntry.FailureMessage = "Starting with Lenddo retry, initial log entry to block other threads from entering retry logic";
            LenddoRetryAuditEntry.AttemptCount = -1;
            RetryAuditRespository.Add(LenddoRetryAuditEntry);
            while (retryCount < lenddoRetryTaskConfiguration.LenddoRetryConfig.MaxAttempts)
            {
                try
                {
                    Logger.Info("Lenddo Report Pulled Requested");
                    var LenddoCreditReportService = LenddoServiceClientFactory.Create(reader);
                    var tryResponse = LenddoCreditReportService.GetClientScore(data.EntityType, data.EntityId, data.Request.ToString());
                    if (!string.IsNullOrEmpty(tryResponse.Result.Score.ToString()))
                    {
                        LenddoRetryAuditEntry.IsSuccess = true;
                        LenddoRetryAuditEntry.AuditTime = TenantTime.Now;
                        LenddoRetryAuditEntry.FailureMessage = string.Empty;
                        LenddoRetryAuditEntry.AttemptCount = retryCount;
                        RetryAuditRespository.Add(LenddoRetryAuditEntry);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    if (retryCount == lenddoRetryTaskConfiguration.LenddoRetryConfig.MaxAttempts - 1)
                    {
                         RetryAuditRespository.RemoveAuditEntry(data.EntityId, data.EntityType, @event.TenantId, data.Request.ToString());
                    }
                    Logger.Error("Lenddo Report Pull Fail", ex);
                    LenddoRetryAuditEntry.IsSuccess = false;
                    LenddoRetryAuditEntry.AuditTime = TenantTime.Now;
                    LenddoRetryAuditEntry.FailureMessage = ex.Message;
                    LenddoRetryAuditEntry.AttemptCount = retryCount;
                    RetryAuditRespository.Add(LenddoRetryAuditEntry);
                }

                retryCount += 1;
                int nextAttemptDelay = 0;
                if (lenddoRetryTaskConfiguration.LenddoRetryConfig.AttemptDelay.Length >= retryCount)
                {
                    nextAttemptDelay = lenddoRetryTaskConfiguration.LenddoRetryConfig.AttemptDelay[retryCount - 1];
                }
                else
                {
                    nextAttemptDelay = lenddoRetryTaskConfiguration.LenddoRetryConfig.AttemptDelay[lenddoRetryTaskConfiguration.LenddoRetryConfig.AttemptDelay.Length - 1];
                }
                Thread.Sleep(nextAttemptDelay);
            }
        }
    }
}
