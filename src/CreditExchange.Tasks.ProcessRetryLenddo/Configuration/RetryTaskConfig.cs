﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryLenddo.Configuration
{
    public class RetryTaskConfig
    {
        public int MaxAttempts { get; set; }
        public int[] AttemptDelay { get; set; }
    }
}
