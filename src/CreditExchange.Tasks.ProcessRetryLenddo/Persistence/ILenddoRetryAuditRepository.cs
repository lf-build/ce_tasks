﻿
using CreditExchange.Tasks.ProcessRetryLenddo.Abstractions;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryLenddo.Persistence
{
    public interface ILenddoRetryAuditRepository : IRepository<ILenddoRetryTaskAudit>
    {
        Task<ILenddoRetryTaskAudit> GetDuplicateRetryEffortDetails(string entityId, string entityType, string tenantId, string clientId);

        Task RemoveAuditEntry(string entityId, string entityType, string tenantId, string clientId);
    }
}