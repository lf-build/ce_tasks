﻿using LendFoundry.Foundation.Persistence.Mongo;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using CreditExchange.Tasks.ProcessRetryLenddo.Abstractions;

namespace CreditExchange.Tasks.ProcessRetryLenddo.Persistence
{
    public class LenddoRetryAuditRepository : MongoRepository<ILenddoRetryTaskAudit, LenddoRetryTaskAudit>, ILenddoRetryAuditRepository
    {
        static LenddoRetryAuditRepository()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(LenddoRetryTaskAudit)))
            {
                BsonClassMap.RegisterClassMap<LenddoRetryTaskAudit>(map =>
                {
                    map.AutoMap();
                    var type = typeof(LenddoRetryTaskAudit);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
        }
        public LenddoRetryAuditRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "lenddo-retry-audit")
        {
            CreateIndexIfNotExists("entityType", Builders<ILenddoRetryTaskAudit>.IndexKeys.Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entityId", Builders<ILenddoRetryTaskAudit>.IndexKeys.Ascending(i => i.EntityId));
            CreateIndexIfNotExists("ClientId", Builders<ILenddoRetryTaskAudit>.IndexKeys.Ascending(i => i.ClientId));
           
        }

        public  Task<ILenddoRetryTaskAudit> GetDuplicateRetryEffortDetails(string entityId, string entityType, string tenantId, string clientId)
        {
            return Task.FromResult<ILenddoRetryTaskAudit>
            (
                Query.FirstOrDefault(x => x.EntityId == entityId &&
                                    x.TenantId == tenantId &&
                                    x.EntityType == entityType &&
                                    x.ClientId == clientId)
            );
        }
        public Task RemoveAuditEntry(string entityId, string entityType, string tenantId, string clientId)
        {

            return Task.Run(() => Collection.DeleteMany(x => x.EntityId == entityId
                                      && x.EntityType == entityType
                                      && x.TenantId == tenantId
                                      && x.ClientId == clientId ));


        }
    }                                         
}
