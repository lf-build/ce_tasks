﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Tasks.ProcessRetryLenddo.Abstractions
{
    public interface ILenddoRetryTaskAudit : IAggregate
    {
         DateTimeOffset AuditTime { get; set; }
         string EntityId { get; set; }
         string EntityType { get; set; }
         string FailureMessage { get; set; }
         string ClientId { get; set; }
         bool IsSuccess { get; set; }
         int AttemptCount { get; set; }
    }
}
