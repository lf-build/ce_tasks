﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Tasks.ProcessRetryLenddo.Abstractions
{
    public class LenddoRetryTaskAudit : Aggregate, ILenddoRetryTaskAudit
    {
        public DateTimeOffset AuditTime { get; set; }

        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string FailureMessage { get; set; }

        public string ClientId { get; set; }

        public bool IsSuccess { get; set; }

      
        public int AttemptCount { get; set; }
    }
}
