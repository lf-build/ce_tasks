using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.ProcessRetryLenddo
{
    public static class Settings
    {
        public static string ServiceName { get; } = "retry-lenddo";

        public static string Prefix = "RETRY-LENDDO";

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings LenddoService { get; } = new ServiceSettings($"{Prefix}_LENDDO_SYNDICATION_HOST", "ce-syndication-lenddo", $"{Prefix}_LENDDO_SYNDICATION_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}