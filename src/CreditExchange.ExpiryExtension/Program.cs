﻿using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Services;
using CreditExchange.ExpiryExtension.Configuration;
using CreditExchange.Application.Client;
using System;

namespace CreditExchange.ExpiryExtension
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
           Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IExpiryExtensionProcessor>().Start();
            Console.WriteLine("Application terminated");
        }
       
        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);                    
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddConfigurationService<ExpiryExtensionConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddTransient<IExpiryExtensionProcessor, ExpiryExtensionProcessor>();        
            return services;
        }
    }
}