﻿using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using CreditExchange.Application.Client;
using CreditExchange.ExpiryExtension.Configuration;
using System;
using CreditExchange.Application;
using LendFoundry.Foundation.Services;
using System.Threading;
using LendFoundry.Configuration;

namespace CreditExchange.ExpiryExtension
{
    public class ExpiryExtensionProcessor : IExpiryExtensionProcessor
    {
        private static readonly ManualResetEvent _waiter = new ManualResetEvent(false);
        public ExpiryExtensionProcessor
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IApplicationServiceClientFactory applicationServiceClientFactory                   
        )
        {           
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ApplicationServiceClientFactory = applicationServiceClientFactory;
        }
        
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ILoggerFactory LoggerFactory { get; }

        private IApplicationServiceClientFactory ApplicationServiceClientFactory { get; }
        
        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Expiry Extension Processor...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);                
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();                
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var eventhub = EventHubFactory.Create(reader);
                    var applicationService = ApplicationServiceClientFactory.Create(reader);

                    var configuration = ConfigurationFactory.Create<ExpiryExtensionConfiguration>(Settings.ServiceName, reader).Get();

                    if (configuration == null)
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    else
                    {                        
                        logger.Info("Starting Expiry extension processor");

                        configuration
                            .ExpiryExtensionEvents                            
                            .ForEach(eventConfig => eventhub.On(eventConfig.EventName, ProcessExpiryExtension(eventConfig, applicationService, logger)));

                        logger.Info("-------------------------------------------");
                        eventhub.StartAsync();
                    }
                });                
                logger.Info("Event Driven Email listener started");
            }            
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process Events for email", ex);
                logger.Info("\nEvent Driven Email is working yet and waiting new event\n");
                Start();
            }
            Console.CancelKeyPress += (e, a) => _waiter.Set();
            _waiter.WaitOne();
        }

        private static Action<EventInfo> ProcessExpiryExtension(ExpiryEventConfiguration eventConfig, IApplicationService applicationService, ILogger logger)
        {
            return async @event =>
            {
                try
                {
                    logger.Info($"Now processing {@event.Name} with payload {@event.Data}");
                    var applicationNumber = eventConfig.ApplicationNumber.FormatWith(@event);
                    var newStatus = eventConfig.StatusPath.FormatWith(@event);

                    var expiryExtensionConfig = eventConfig.ExpiryConfig.Find(x => x.StatusCode == newStatus);

                    if(expiryExtensionConfig != null)
                    {
                        await applicationService.ExtendApplicationExpiry(applicationNumber, expiryExtensionConfig.ExtendByDays, expiryExtensionConfig.ShouldExtendOnExpiryDate);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while listening event {@event.Name}", ex);
                }
            };
        }
    }
}