﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.ExpiryExtension.Configuration
{
    public class ExpiryStatusConfiguration
    {
        public string StatusCode { get; set; }
        public int ExtendByDays { get; set; }
        public bool ShouldExtendOnExpiryDate { get; set; }
    }
}
