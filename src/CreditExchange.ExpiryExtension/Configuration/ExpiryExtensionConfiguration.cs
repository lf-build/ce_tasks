﻿using System.Collections.Generic;

namespace CreditExchange.ExpiryExtension.Configuration
{
    public class ExpiryExtensionConfiguration
    {
        public List<ExpiryEventConfiguration> ExpiryExtensionEvents { get; set; }
    }
}
