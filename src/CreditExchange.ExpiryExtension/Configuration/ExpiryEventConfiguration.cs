﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.ExpiryExtension.Configuration
{
    public class ExpiryEventConfiguration
    {
        public string EventName { get; set; }
        public string ApplicationNumber { get; set; }
        public string StatusPath { get; set; }
        public List<ExpiryStatusConfiguration> ExpiryConfig { get; set; }
    }
}
