﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.EventHub.Client;
using LendFoundry.DataAttributes.Client;
using CreditExchange.Applicant.Client;
using CreditExchange.Application.Client;
using LendFoundry.ProductConfiguration.Client;
using CreditExchange.Applications.Filters.Client;
using System;
using CreditExchange.ScoreCard.Client;

namespace CreditExchange.Tasks.ReGenerateDataAttribute
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);                
            services.AddApplicantService(Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddApplicationsFilterService(Settings.ApplicationFilters.Host, Settings.ApplicationFilters.Port);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddProductService(Settings.Product.Host, Settings.Product.Port);
            services.AddScoreCardService(Settings.Scorecard.Host, Settings.Scorecard.Port);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}