using System.Collections.Generic;

namespace CreditExchange.Tasks.ReGenerateDataAttribute
{
    public class Configuration
    {
        public string DefaultProduct { get; set; }
        public bool MigrateScorecardResultToDataAttributes { get; set; }
        public List<string> MigrateToDataAttributesNames { get; set; }
    }
}