using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using CreditExchange.Applicant.Client;
using CreditExchange.Application;
using CreditExchange.Application.Client;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.ScoreCard.Client;
using CreditExchange.Tasks.Agent;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.Security.Tokens;
using Newtonsoft.Json.Linq;
using CreditExchange.ScoreCard;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ReGenerateDataAttribute {
    public class Agent : ScheduledAgent {
        public Agent
            (
                IConfigurationServiceFactory configurationFactory,
                ITokenHandler tokenHandler,
                IEventHubClientFactory eventHubFactory,
                ITenantTimeFactory tenantTimeFactory,
                IDataAttributesClientFactory dataAttributesEngineFactory,
                ILoggerFactory loggerFactory,
                IApplicantServiceClientFactory applicantServiceClientFactory,
                IApplicationServiceClientFactory applicationServiceClientFactory,
                IProductServiceFactory productServiceFactory,
                IApplicationFilterClientServiceFactory applicationFilterClientFactory,
                IScoreCardServiceClientFactory scoreCardServiceClientFactory) : base (tokenHandler, configurationFactory, tenantTimeFactory) {
                ConfigurationFactory = configurationFactory;
                TokenHandler = tokenHandler;
                TenantTimeFactory = tenantTimeFactory;
                DataAttributesEngineFactory = dataAttributesEngineFactory;
                LoggerFactory = loggerFactory;
                EventHubFactory = eventHubFactory;
                ApplicationServiceClientFactory = applicationServiceClientFactory;
                ApplicantServiceClientFactory = applicantServiceClientFactory;
                ProductServiceFactory = productServiceFactory;
                ApplicationFilterClientFactory = applicationFilterClientFactory;
                ScoreCardServiceClientFactory = scoreCardServiceClientFactory;
            }

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IDataAttributesClientFactory DataAttributesEngineFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private IApplicantServiceClientFactory ApplicantServiceClientFactory { get; set; }
        private IApplicationServiceClientFactory ApplicationServiceClientFactory { get; set; }
        private IProductServiceFactory ProductServiceFactory { get; set; }
        private IApplicationFilterClientServiceFactory ApplicationFilterClientFactory { get; }
        private IScoreCardServiceClientFactory ScoreCardServiceClientFactory { get; }

        #endregion

        public override async void OnSchedule () {
            var token = TokenHandler.Issue (Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader (token.Value);
            var logger = LoggerFactory.Create (NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create (ConfigurationFactory, reader);
            var dataAttribute = DataAttributesEngineFactory.Create (reader);
            var applicant = ApplicantServiceClientFactory.Create (reader);
            var application = ApplicationServiceClientFactory.Create (reader);
            var eventHub = EventHubFactory.Create (reader);
            var productService = ProductServiceFactory.Create (reader);
            var applicationFilter = ApplicationFilterClientFactory.Create (reader);
            var scoreCardService = ScoreCardServiceClientFactory.Create (reader);
            var configuration = ConfigurationFactory.Create<Configuration> (Settings.ServiceName, reader);
            var regenerateDataAttributesConfiguration = configuration.Get ();
            if (regenerateDataAttributesConfiguration == null) {
                logger.Error ("Configuration was not found for regenerate data-attributes");
                return;
            }
            try {
                logger.Info ("Start regenerate data-attributes");
                var applications = applicationFilter.GetAll ();
                //var statues = new List<string> { "200.01", "200.02", "200.03", "200.04", "200.05", "200.06", "200.07", "200.08", "200.09", "200.10", "200.11", "200.12", "200.14", "200.15", "200.16" };
                //var applications = await applicationFilter.GetAllByStatus(statues);
                if (applications == null)
                    return;
                foreach (var item in applications) {
                    if (item == null)
                        continue;
                    //Update application data attributes including product id
                    var applicationData = await application.GetByApplicationNumber (item.ApplicationNumber);
                    var applicantData = await applicant.Get (applicationData.ApplicantId);
                    await eventHub.Publish (new ApplicationModified { Application = applicationData, Applicant = applicantData });

                    // Add product data attributes if not found
                    var productDataAttributeExists = (JArray) await dataAttribute.GetAttribute ("application", item.ApplicationNumber, "product");
                    if (productDataAttributeExists == null || productDataAttributeExists.Count == 0) {
                        var products = await productService.GetAll ();
                        if (products == null)
                            continue;

                        var product = products.Where (p => p.ProductId == regenerateDataAttributesConfiguration.DefaultProduct).FirstOrDefault ();
                        if (product == null)
                            continue;

                        var productGroup = await productService.GetAllProductGroup (product.ProductId);
                        var productParameters = await productService.GetAllProductParameters (product.ProductId);
                        var productFeeParameters = await productService.GetAllProductFeeParameters (product.ProductId);
                        dynamic productData = new ExpandoObject ();
                        productData.ProductId = product.ProductId;
                        productData.Product = product;
                        productData.ProductGroup = productGroup;
                        productData.ProductParameter = productParameters;
                        productData.ProductFeeParameters = productFeeParameters;
                        await dataAttribute.SetAttribute ("application", item.ApplicationNumber, "product", (object) productData);
                    }
                    if (regenerateDataAttributesConfiguration.MigrateScorecardResultToDataAttributes && regenerateDataAttributesConfiguration.MigrateToDataAttributesNames != null && regenerateDataAttributesConfiguration.MigrateToDataAttributesNames.Any ()) {
                        List<IScoreCardRuleResult> scoreCardResult = null;
                        try {
                            scoreCardResult = await scoreCardService.GetScoreCardResultDetail ("application", item.ApplicationNumber);
                        } catch (Exception) {

                        }

                        if (scoreCardResult != null) {
                            var executedRules = scoreCardResult.Where (sc => regenerateDataAttributesConfiguration.MigrateToDataAttributesNames.Contains (sc.Name, StringComparer.InvariantCultureIgnoreCase))
                                .GroupBy (r => r.Name)
                                .Select (g => g.OrderByDescending (x => x.ExecutedDate).First ())
                                .ToList ();
                            if (executedRules == null)
                                continue;
                            var allDataAttributes = await dataAttribute.GetAllAttributes ("application", item.ApplicationNumber);
                            foreach (var executedRule in executedRules) {
                                if (executedRule == null)
                                    continue;
                                if (allDataAttributes != null) {
                                    try {
                                        if (allDataAttributes[GetName (executedRule.Name)] != null)
                                            continue;
                                    } catch (Exception) { }

                                }
                                var intermediateResult = new Dictionary<string, object> { { executedRule.Name, executedRule.IntermediateData } };
                                await dataAttribute.SetAttribute ("application", item.ApplicationNumber, executedRule.Name, executedRule.IntermediateData);
                            }
                        }
                    }
                }

                logger.Info ("End regenerate data-attributes");
            } catch (Exception ex) {
                logger.Error ($"Failed attempt to process regenerate data-attributes", ex);
            }
        }

        private string GetName (string name) {
            return char.ToLower (name[0]) + name.Substring (1);
        }
    }
}