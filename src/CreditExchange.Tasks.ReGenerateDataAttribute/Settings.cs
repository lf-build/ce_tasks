using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.ReGenerateDataAttribute
{
    public static class Settings
    {
        public static string ServiceName { get; } = "regenerate-data-attributes";

        public static string Prefix = "REGENERATE-DATA-ATTRIBUTES";
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATA-ATTRIBUTES", "data-attributes");
        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION", "application");
        public static ServiceSettings ApplicationFilters { get; } = new ServiceSettings($"{Prefix}_APPLICATION-FILTERS", "application-filters");
        public static ServiceSettings Applicant { get; } = new ServiceSettings($"{Prefix}_APPLICANT", "applicant");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");
        public static ServiceSettings Product { get; } = new ServiceSettings($"{Prefix}_PRODUCT", "product");
        public static ServiceSettings Scorecard { get; } = new ServiceSettings($"{Prefix}_SCORECARD", "scorecard");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}