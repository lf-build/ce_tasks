using System.Collections.Generic;
using System.Threading;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
using Quartz;
using Quartz.Impl;
using LendFoundry.Configuration;
#if DOTNET2
using System.Threading.Tasks;
#endif

namespace CreditExchange.Tasks.Agent
{
    public abstract class ScheduledAgent : IAgent
    {
        protected ScheduledAgent(
            ITokenHandler tokenHandler, 
            IConfigurationServiceFactory configurationFactory, 
            ITenantTimeFactory tenantTimeFactory)
        {
            TokenHandler = tokenHandler;
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            Tenant = Settings.Tenant;
            Schedule = Settings.Schedule;
        }

        private ITokenHandler TokenHandler { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        protected string Tenant { get; }

        private string Schedule { get; }

        public void Execute()
        {
#if DOTNET2
            var scheduler = new StdSchedulerFactory().GetScheduler().Result;
#else
            var scheduler = new StdSchedulerFactory().GetScheduler();
#endif
            scheduler.ScheduleJob(CreateJob(), CreateTrigger());
            scheduler.Start();
            while (!scheduler.IsShutdown) Thread.Sleep(100);
        }

        public abstract void OnSchedule();

        private IJobDetail CreateJob()
        {
            var data = new Dictionary<string, object>
            {
                {"agent", this}
            } as IDictionary<string, object>;

            return JobBuilder
                .Create<AgentJob>()
                .SetJobData(new JobDataMap(data))
                .Build();
        }

        private ITrigger CreateTrigger()
        {
            var token = TokenHandler.Issue(Tenant, string.Empty);
            var reader = new StaticTokenReader(token.Value);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);


            var expression = new CronExpression(Schedule) {TimeZone = tenantTime.TimeZone};
            var schedule = CronScheduleBuilder.CronSchedule(expression);
            var triggerBuilder = TriggerBuilder.Create().WithSchedule(schedule);
            var trigger = triggerBuilder.Build();
            return trigger;
        }

        private class AgentJob : IJob
        {
#if DOTNET2
            public Task Execute(IJobExecutionContext context)
            {
                return Task.Run(() =>
                {
                    var agent = context.MergedJobDataMap.Get("agent") as ScheduledAgent;
                    agent?.OnSchedule();
                });
            }
#else
            public void Execute(IJobExecutionContext context)
            {
                var agent = context.MergedJobDataMap.Get("agent") as ScheduledAgent;
                agent?.OnSchedule();
            }
#endif
        }
    }
}