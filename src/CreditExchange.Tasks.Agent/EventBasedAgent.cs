﻿using System;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Security.Tokens;
using System.Threading;

namespace CreditExchange.Tasks.Agent
{
    public abstract class EventBasedAgent : IEventBasedAgent
    {
        private const string TenantEnvironmentVariable = "TASK_TENANT";
        private static readonly ManualResetEvent _waiter = new ManualResetEvent(false);
        protected EventBasedAgent(IEventHubClientFactory factory, string eventName, ITokenHandler tokenHandler, string serviceName)
        {
            EventHubFactory = factory;
            EventName = eventName;
            Tenant = Environment.GetEnvironmentVariable(TenantEnvironmentVariable);
            TokenHandler = tokenHandler;
            ServiceName = serviceName;
            
        }

        protected IEventHubClientFactory EventHubFactory { get; }

        private string EventName { get; }
       
        protected string Tenant { get; }

        protected ITokenHandler TokenHandler { get; }

        protected string ServiceName { get; }

        protected ITokenReader TokenReader { get; }

        public void Execute()
        {
            var token = TokenHandler.Issue(Tenant, ServiceName);
            var hub = EventHubFactory.Create(new StaticTokenReader(token.Value));
            hub.On(EventName, @event => { Execute(@event); });
            hub.StartAsync();
            Console.CancelKeyPress += (e, a) => _waiter.Set();
            _waiter.WaitOne();
        }

        public abstract void Execute(EventInfo @event);
    }
}
