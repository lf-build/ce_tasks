#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace CreditExchange.Tasks
{
    public interface IEventBasedAgent : IAgent
    {
        
        void Execute(EventInfo @event);
    }
}