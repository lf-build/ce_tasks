using System.Collections.Generic;

namespace CreditExchange.Tasks
{
    public interface ITask
    {
        string Tenant { get; set; }

        string Name { get; set; }

        string Image { get; set; }

        Dictionary<string, string> Environment { get; set; }

        Dictionary<string, string> Links { get; set; }

        string Expression { get; set; }
    }
}