using LendFoundry.Security.Tokens;

namespace CreditExchange.Tasks
{
    public interface ITaskRunnerFactory
    {
        ITaskRunner Create(ITokenReader reader);
    }
}