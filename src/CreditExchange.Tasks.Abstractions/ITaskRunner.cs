using System.Threading.Tasks;

namespace CreditExchange.Tasks
{
    public interface ITaskRunner
    {
        Task<bool> Run(ITask task);
    }
}