namespace CreditExchange.Tasks
{
    public interface IAgent
    {
        void Execute();
    }
}