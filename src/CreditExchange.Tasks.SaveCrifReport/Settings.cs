using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.SaveCrifReport
{
    public static class Settings
    {
        public static string ServiceName { get; } = "save-crif-document";
        public static string Prefix = "save-crif-document".ToUpper();
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings($"{Prefix}_APPLICATION_DOCUMENT_HOST", "ce-application-document", $"{Prefix}_APPLICATION_DOCUMENT_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";

    }
}