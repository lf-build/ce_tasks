using System;
using CreditExchange.Tasks.Agent;
using LendFoundry.Configuration.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using CreditExchange.Syndication.Crif.Events;
using Newtonsoft.Json;
using LendFoundry.Application.Document.Client;
using CreditExchange.Syndication.Crif.CreditReport.Response;
using System.Text;
using LendFoundry.Application.Document;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.SaveCrifReport
{
    public class SaveCRIFReportAgent : EventBasedAgent
    {
        public SaveCRIFReportAgent
        (
            IConfigurationServiceFactory configurationServiceFactory,
            IEventHubClientFactory eventHubFactory,
             ITokenHandler tokenHandler,
             ITokenReaderFactory tokenHandlerFactory,
            IApplicationDocumentServiceClientFactory applicationDocumentServiceClientFactory,
             ILogger logger
            )
            : base(eventHubFactory, nameof(CrifCreditReportPulled), tokenHandler, Settings.ServiceName)
        {
            ApplicationDocumentServiceClientFactory = applicationDocumentServiceClientFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            Logger = logger;
        }

        private ILogger Logger { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private IApplicationDocumentServiceClientFactory ApplicationDocumentServiceClientFactory { get; }
        public override async void Execute(EventInfo @event)
        {
            var data = @event.Cast<CrifCreditReportPulled>();
            try
            {
                if (data.Response != null)
                {
                    var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var configurationService = ConfigurationServiceFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
                    var configuration = configurationService.Get();
                    if (configuration == null)
                    {
                        Logger.Error($"No configuration found for save crif report task for tenant #{Tenant}");
                        return;
                    }

                    IApplicationDocumentService applicationdocumentservice = ApplicationDocumentServiceClientFactory.Create(reader);
                    var creditreport = JsonConvert.DeserializeObject<GetCreditReportResponse>(data.Response.ToString());
                    if (creditreport?.IndividualReponseList?.Printablereport != null)
                    {
                        var htmlreport = Encoding.Default.GetBytes(creditreport?.IndividualReponseList?.Printablereport?.Content);
                        var filename = creditreport.IndividualReponseList.Printablereport.Filename;
                        var response = await applicationdocumentservice.Add(data.EntityId, configuration.DocumentCategoryName, htmlreport, filename, null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
