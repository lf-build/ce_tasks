﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.HunterFileExport
{
    public interface ICsvGenerator
    {
        string Delimiter { get; set; }
        byte[] WriteToCsv<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection);
    }
}