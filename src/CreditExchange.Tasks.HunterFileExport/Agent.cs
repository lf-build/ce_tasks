using System;
using System.Linq;
using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Tasks.Agent;
using CreditExchange.Applications.Filters.Abstractions.Services;
using System.Collections.Generic;
using System.IO;
using LendFoundry.Clients.DecisionEngine;

using CreditExchange.Sftp;

using LendFoundry.DocumentManager.Client;
using LendFoundry.DocumentManager;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.HunterFileExport
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IApplicationFilterClientServiceFactory applicationFilterClientFactory,
            IDataAttributesClientFactory dataAttributesEngineFactory,
            ICsvGenerator csvGenerator,
            IDecisionEngineClientFactory decisionEngineServiceFactory,
            ILoggerFactory loggerFactory,
            IFileStorageService fileStorageService,
            IDocumentManagerServiceFactory documentServiceFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            ApplicationFilterClientFactory = applicationFilterClientFactory;
            LoggerFactory = loggerFactory;
            CsvGenerator = csvGenerator;
            DataAttributesEngineFactory = dataAttributesEngineFactory;
            DecisionEngineServiceFactory = decisionEngineServiceFactory;
            EventHubFactory = eventHubFactory;
            FileStorageService = fileStorageService;
            DocumentServiceFactory = documentServiceFactory;
        }

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IApplicationFilterClientServiceFactory ApplicationFilterClientFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ICsvGenerator CsvGenerator { get; }
        private IDecisionEngineClientFactory DecisionEngineServiceFactory { get; }
        private IDataAttributesClientFactory DataAttributesEngineFactory { get; }
        private IFileStorageService FileStorageService { get; }
        private IDocumentManagerServiceFactory DocumentServiceFactory { get; }

        #endregion

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var localTenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            IApplicationFilterService applicationFilter = ApplicationFilterClientFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var dataAttributesEngine = DataAttributesEngineFactory.Create(reader);
            var decisionEngineService = DecisionEngineServiceFactory.Create(reader);
            var documentManager = DocumentServiceFactory.Create(reader);

            var configuration = ConfigurationFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to application filter Export status Task");
                return;
            }

            var applicationExportConfiguration = configuration.Get();
            if (applicationExportConfiguration == null)
            {
                logger.Error("Hunter file export configuration not found.");
                return;
            }
            if (applicationExportConfiguration.CEAprovalExportconfig == null)
            {
                logger.Error("Didn't find Approval status related configuration for hunter file export.");
                return;
            }

            if (string.IsNullOrWhiteSpace(applicationExportConfiguration.CEAprovalExportconfig.ExportTag))
            {
                logger.Warn("No Approval status code found for Hunter file export.");
                return;
            }

            try
            {
                List<string> tags = new List<string>();
                List<string> appsToUnTag = new List<string>();
                List<HunterDataView> batchRecords = new List<HunterDataView>();
                IDocument uploadedDocument = null;
                tags.Add(applicationExportConfiguration.CEAprovalExportconfig.ExportTag);
                var AllApplicationsToBeExported = await applicationFilter.GetAllByTag(tags);

                logger.Info("Start Export manager task");
                if (AllApplicationsToBeExported != null && AllApplicationsToBeExported.Any())
                {
                    logger.Info("Start for each approved application");
                    foreach (var ApprovedApplication in AllApplicationsToBeExported)
                    {
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(ApprovedApplication.ApplicationNumber))
                            {
                                var dataAttributes = await dataAttributesEngine.GetAllAttributes("application", ApprovedApplication.ApplicationNumber);
                                var payload = new { DataAttributes = dataAttributes };
                                if (dataAttributes != null && dataAttributes.Keys.Contains("selectedFinalOffer"))
                                {
                                    var ruleResult = decisionEngineService.Execute<dynamic, HunterDataView>(applicationExportConfiguration.CEAprovalExportconfig.ExportHunterRule.Name,
                                                                                                            new { input = payload });

                                    if (ruleResult != null)
                                    {
                                        batchRecords.Add(ruleResult);
                                        appsToUnTag.Add(ApprovedApplication.ApplicationNumber);
                                    }
                                    else
                                    {
                                        logger.Error($"Export of hunter record for application {ApprovedApplication.ApplicationNumber} failed.");
                                    }
                                }
                                else
                                {
                                    throw new Exception($"No attributes found for application with number : {ApprovedApplication.ApplicationNumber}");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Warn($"Error occurred while exporting application with identifier {ApprovedApplication.ApplicationNumber}" + ex);
                        }
                    }

                    if (batchRecords.Count() > 0)
                    {
                        try
                        {
                            logger.Info("Starting to create hunter file");
                            var csvBytes = CsvGenerator.WriteToCsv(batchRecords, applicationExportConfiguration.CEAprovalExportconfig.CsvProjection);
                            var csvGeneratedFileName = $"HunterExport_{localTenantTime.Now.ToString("s")}.csv";
                            var docManagerEntityId = Guid.NewGuid().ToString("N");

                            using (var csvMemoryStream = new MemoryStream(csvBytes))
                            {
                                FileStorageService.Upload(csvMemoryStream, csvGeneratedFileName, applicationExportConfiguration.CEAprovalExportconfig.HunterFileStoragePath);
                            }

                            using (var csvMemoryStream = new MemoryStream(csvBytes))
                            {
                                try
                                {
                                    uploadedDocument = await documentManager.Create(csvMemoryStream, "exportedfiles", docManagerEntityId, csvGeneratedFileName);
                                }
                                catch (Exception ex) { }
                            }

                            foreach (var app in appsToUnTag)
                            {
                                applicationFilter.UnTagWithoutEvaluation(app, tags);
                                applicationFilter.TagsWithoutEvaluation(app, new List<string> { applicationExportConfiguration.CEAprovalExportconfig.PostExportTag });
                                if (uploadedDocument != null)
                                {
                                    applicationFilter.AttachHunterDocumentID(app, docManagerEntityId, uploadedDocument.Id);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error($"Error while processing CSV ", ex);
                            return;
                        }
                        finally
                        {
                            logger.Info("End");
                        }
                    }

                    logger.Info("End for each approved application");
                }
                else
                    logger.Info("No application has been approved yet.");
                logger.Info("end Export manager task");
            }
            catch (Exception ex)
            {
                logger.Error($"Failed attempt to process export to hunter file.", ex);
            }
        }
    }
}