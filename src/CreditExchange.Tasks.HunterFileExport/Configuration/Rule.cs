﻿namespace CreditExchange.Tasks.HunterFileExport.Configuration
{
    public class Rule
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
