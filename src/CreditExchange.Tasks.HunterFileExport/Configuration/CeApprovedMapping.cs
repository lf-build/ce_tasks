﻿using CreditExchange.Sftp.Configuration;
using System.Collections.Generic;

namespace CreditExchange.Tasks.HunterFileExport.Configuration
{
    public class CeApprovedMapping
    {
        public string ExportTag { get; set; }
        public string PostExportTag { get; set; }  
        public HunterFileExport.Configuration.Rule ExportHunterRule { get; set; }
        public Dictionary<string, string> CsvProjection { get; set; }
        public string HunterFileStoragePath { get; set; }
        public SftpProviderConfiguration SftpConfiguration { get; set; }
    }
}