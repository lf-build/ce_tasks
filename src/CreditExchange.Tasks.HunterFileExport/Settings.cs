using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.HunterFileExport
{
    public static class Settings
    {
        public static string ServiceName { get; } = "hunter-file-export";
        public static string Prefix = "HUNTER-FILE-EXPORT";

        public static string Tenant => Environment.GetEnvironmentVariable("TASK_TENANT") ?? "my-tenant";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");        
        public static ServiceSettings ApplicationFilter { get; } = new ServiceSettings($"{Prefix}_APPLICATIONFILTER_HOST", "application-filters", $"{Prefix}_APPLICATIONFILTER_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATA_ATTRIBUTES", "data-attributes");
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE", "decision-engine");
        public static ServiceSettings DocumentManager { get; } = new ServiceSettings($"{Prefix}_DOCUMENT_MANAGER", "document-manager");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}