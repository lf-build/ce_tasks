﻿using System;

namespace CreditExchange.Tasks.HunterFileExport
{
    public class HunterDataView : IHunterDataView
    {
        public string Month { get; } = string.Empty;
        public string BatchNumber { get; } = string.Empty;
        public string Identifier { get; } = string.Empty;
        public string Product { get; } = string.Empty;
        public string Classification { get; } = string.Empty;
        public string DateOfApplication { get; set; }
        public string FileCreationDate { get; set; }
        public int LoanTenureInMonths { get; set; }
        public double LoanAmountApplied { get; set; }
        public double ApprovedLoanAmount { get; set; }
        public string PanNumber { get; set; }
        public string ApplicantFirstName { get; set; }
        public string ApplicantMiddleName { get; set; }
        public string ApplicantLastName { get; set; }
        public string ApplicantDOB { get; set; }
        public string ApplicantResiAddress { get; set; }
        public string ApplicantResiCity { get; set; }
        public string ApplicantResiState { get; set; }
        public string ApplicantResiCountry { get; set; } // Defaults to India only - though will read this from Applicant Data
        public string ApplicantResiPinCode { get; set; }
        public string ApplicantMobileNumber { get; set; } // Refered to as Home Telephone number in Hunterfile format
        public string ApplicantMobilePhoneNumber { get; set; } 
        public string ApplicantBusinessPhoneNumber { get; set; } 
        public string ApplicantSalAccBank { get; set; }
        public string ApplicantSalAccNumber { get; set; }
        public string ApplicantIdDocType { get; set; }
        public string ApplicantIdDocNumber { get; set; }
        public string ApplicantAltIdDocType { get; set; } //For KYC apart from Passport to be written as “GOVERNMENT ID CARD”
        public string ApplicantAltIdDocNumber { get; set; }
        public string ApplicantEmployerName { get; set; }
        public string ApplicantEmploymentAddress { get; set; }
        public string ApplicantEmploymentCity { get; set; }
        public string ApplicantEmploymentState { get; set; }
        public string ApplicantEmploymentCountry { get; set; } // Defaults to India only - though will read this from Applicant Data
        public string ApplicantEmploymentPinCode { get; set; }
        public string UploadedDate { get; set; }
        public string HunterMatchStatus { get; set; }
        public string Suspect { get; set; }
        public string Reason { get; set; }
        public string ClosureDate { get; set; }
        public string DeviationTakenby { get; set; }
        public string DeviationMitigate { get; set; }
        public string ifofmatch { get; set; }
    }
}
