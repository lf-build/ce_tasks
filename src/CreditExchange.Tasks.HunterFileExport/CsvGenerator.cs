﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CreditExchange.Tasks.HunterFileExport
{
    public class CsvGenerator : ICsvGenerator
    {
        public string Delimiter { get; set; } = "|";
        private Dictionary<string, string> CsvProjection { get; set; }

        public byte[] WriteToCsv<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection)
        {
            try
            {
                CsvProjection = csvProjection;
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream))
                    using (var csvWriter = new CsvWriter(streamWriter, SetCsvProjectionValues()))
                    {
                        csvWriter.WriteRecords(items);
                    }
                    return memoryStream.ToArray();
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CsvConfiguration SetCsvProjectionValues()
        {
            var csvConfiguration = new CsvConfiguration()
            {
                Encoding = Encoding.UTF8,
                IgnoreBlankLines = true,
                Delimiter = Delimiter,
                QuoteAllFields = true
            };

            if (CsvProjection != null && CsvProjection.Any())
                csvConfiguration.RegisterClassMap(SetCsvClassMap());
            
            return csvConfiguration;
        }

        private CsvClassMap SetCsvClassMap()
        {
            var FileMap = new DefaultCsvClassMap<HunterDataView>();
            CsvClassMap csvFileMap;
            Dictionary<string, string> csvProjection = CsvProjection;
            var columnIndex = 0;
            foreach (var csvKeyColumn in csvProjection)
            {
                var schemaPropertyName = csvKeyColumn.Key;
                var csvColumnName = csvKeyColumn.Value;

                if (!String.IsNullOrEmpty(csvColumnName))
                {
                    var propertyInfo = typeof(HunterDataView).GetProperty(schemaPropertyName);
                    var newMap = new CsvPropertyMap(propertyInfo);
                    newMap.Name(csvColumnName);
                    newMap.Index(columnIndex);
                    FileMap.PropertyMaps.Add(newMap);
                    columnIndex += 1;
                }
            }

            csvFileMap = FileMap;
            return csvFileMap;
        }
    }
}