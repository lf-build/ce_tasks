﻿namespace CreditExchange.Tasks.HunterFileExport
{
    interface IHunterDataView
    {
        string Month { get; }
        string BatchNumber { get; }
        string Identifier { get; }
        string Product { get; }
        string Classification { get; }
        string DateOfApplication { get; set; }
        string FileCreationDate { get; set; }
        int LoanTenureInMonths { get; set; }
        double LoanAmountApplied { get; set; }
        double ApprovedLoanAmount { get; set; }
        string PanNumber { get; set; }
        string ApplicantFirstName { get; set; }
        string ApplicantMiddleName { get; set; }
        string ApplicantLastName { get; set; }
        string ApplicantDOB { get; set; }
        string ApplicantResiAddress { get; set; }
        string ApplicantResiCity { get; set; }
        string ApplicantResiState { get; set; }
        string ApplicantResiCountry { get; set; }
        string ApplicantResiPinCode { get; set; }
        string ApplicantMobileNumber { get; set; } // Refered to as Home Telephone number in Hunterfile format
        string ApplicantMobilePhoneNumber { get; }  // This will always have empty string as we don't capture, yet Hunterfile needs this field
        string ApplicantBusinessPhoneNumber { get; } // This will always have empty string as we don't capture, yet Hunterfile needs this field
        string ApplicantSalAccBank { get; set; }
        string ApplicantSalAccNumber { get; set; }
        string ApplicantIdDocType { get; set; }
        string ApplicantIdDocNumber { get; set; }
        string ApplicantAltIdDocType { get; set; } //For KYC apart from Passport to be written as “GOVERNMENT ID CARD”
        string ApplicantAltIdDocNumber { get; set; }
        string ApplicantEmployerName { get; set; }
        string ApplicantEmploymentAddress { get; set; }
        string ApplicantEmploymentCity { get; set; }
        string ApplicantEmploymentState { get; set; }
        string ApplicantEmploymentCountry { get; set; }
        string ApplicantEmploymentPinCode { get; set; }
        string UploadedDate { get; set; }
		string HunterMatchStatus { get; set; }
	    string Suspect { get; set; }
		string Reason { get; set; }
        string ClosureDate { get; set; }
		string DeviationTakenby { get; set; }
		string DeviationMitigate { get; set; }
		string ifofmatch { get; set; }
    }
}
