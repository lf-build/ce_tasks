using System;
using CreditExchange.Crif.CreditReport.Client;
using CreditExchange.Syndication.Crif.Events;
using CreditExchange.Tasks.Agent;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using CreditExchange.Tasks.ProcessRetryCRIF.Persistence;
using System.Threading;
using CreditExchange.Tasks.ProcessRetryCRIF.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.ProcessRetryCRIF
{
    public class CRIFAgent : EventBasedAgent
    {
        public CRIFAgent
        (
            IEventHubClientFactory eventHubFactory,
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
             ITokenReaderFactory tokenHandlerFactory,
            ITenantTimeFactory tenantTimeFactory,
            ICrifCreditReportServiceFactory crifCreditReportServiceFactory,
            ITenantServiceFactory tenantServiceFactory,
            IMongoConfiguration mongoConfiguration,
            ILogger logger
            )
            : base(eventHubFactory, nameof(CreditReportPullFail), tokenHandler, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            CrifCreditReportServiceFactory = crifCreditReportServiceFactory;
            TenantServiceFactory = tenantServiceFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            MongoConfiguration = mongoConfiguration;
            Logger = logger;
        }

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ICrifCreditReportServiceFactory CrifCreditReportServiceFactory { get; }
        private ILogger Logger { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IMongoConfiguration MongoConfiguration { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }

        #endregion

        public override async void Execute(EventInfo @event)
        {
            var reader = TokenHandlerFactory.Create(@event.TenantId, string.Empty);
            var configuration = ConfigurationFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
            var TenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var tenantService = TenantServiceFactory.Create(reader);
            var RetryAuditRespository = new CRIFRetryAuditRepository(tenantService, MongoConfiguration);

            if (configuration == null)
            {
                Logger.Error("Was not found the Configuration related to CRIF retry Task");
                return;
            }

            var data = @event.Cast<CreditReportPullFail>();

            if (data?.Request == null)
            {
                Logger.Error("Request parameters are missing from Crif Report pull retry notification");
            }

            var reportParams = JsonConvert.DeserializeObject<CrifReportPullRequest>(data.Request.ToString());

            var ongoingRetry = await RetryAuditRespository.GetDuplicateRetryEffortDetails(data.EntityId, data.EntityType, @event.TenantId, reportParams.inquiryReferenceNumber, reportParams.reportId);

            if (ongoingRetry != null)
            {
                return; // exit as retry from another thread is already in progress.
            }

            var CrifRetryAuditEntry = new CRIFRetryTaskAudit()
            {
                EntityId = data.EntityId,
                EntityType = data.EntityType,
                TenantId = @event.TenantId,
                InquiryReferenceNumber = reportParams.inquiryReferenceNumber,
                ReportId = reportParams.reportId
            };

            var crifRetryTaskConfiguration = configuration.Get();
            int retryCount = 0;
            CrifRetryAuditEntry.IsSuccess = false;
            CrifRetryAuditEntry.AuditTime = TenantTime.Now;
            CrifRetryAuditEntry.FailureMessage = "Starting with Crif retry, initial log entry to block other threads from entering retry logic";
            CrifRetryAuditEntry.AttemptCount = -1;
            RetryAuditRespository.Add(CrifRetryAuditEntry);
            while (retryCount < crifRetryTaskConfiguration.CRIFRetryConfig.MaxAttempts)
            {
                try
                {
                    Logger.Info("Crif Report Pulled Requested");
                    var CrifCreditReportService = CrifCreditReportServiceFactory.Create(reader);
                    var tryResponse = await CrifCreditReportService.GetReport(data.EntityType, data.EntityId, reportParams.inquiryReferenceNumber, reportParams.reportId);
                    if (!string.IsNullOrEmpty(tryResponse.ReferenceNumber))
                    {
                        CrifRetryAuditEntry.IsSuccess = true;
                        CrifRetryAuditEntry.AuditTime = TenantTime.Now;
                        CrifRetryAuditEntry.FailureMessage = string.Empty;
                        CrifRetryAuditEntry.AttemptCount = retryCount;
                        RetryAuditRespository.Add(CrifRetryAuditEntry);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Crif Report Pull Fail", ex);
                    CrifRetryAuditEntry.IsSuccess = false;
                    CrifRetryAuditEntry.AuditTime = TenantTime.Now;
                    CrifRetryAuditEntry.FailureMessage = ex.Message;
                    CrifRetryAuditEntry.AttemptCount = retryCount;
                    RetryAuditRespository.Add(CrifRetryAuditEntry);
                }

                retryCount += 1;
                int nextAttemptDelay = 0;
                if (crifRetryTaskConfiguration.CRIFRetryConfig.AttemptDelay.Length >= retryCount)
                {
                    nextAttemptDelay = crifRetryTaskConfiguration.CRIFRetryConfig.AttemptDelay[retryCount - 1];
                }
                else
                {
                    nextAttemptDelay = crifRetryTaskConfiguration.CRIFRetryConfig.AttemptDelay[crifRetryTaskConfiguration.CRIFRetryConfig.AttemptDelay.Length - 1];
                }
                Thread.Sleep(nextAttemptDelay);
            }
        }
    }
}