﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryCRIF.Abstractions
{
    public interface ICRIFRetryTaskAudit : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string InquiryReferenceNumber { get; set; }
        string ReportId { get; set; }
        bool IsSuccess { get; set; }
        string FailureMessage { get; set; }
        DateTimeOffset AuditTime { get; set; }
    }
}
