﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryCRIF.Abstractions
{
    public class CrifReportPullRequest
    {
        public string inquiryReferenceNumber { get; set; }
        public string reportId { get; set; }
    }
}
