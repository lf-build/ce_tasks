﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryCRIF.Abstractions
{
    public class CRIFRetryTaskAudit : Aggregate, ICRIFRetryTaskAudit
    {
        public DateTimeOffset AuditTime { get; set; }

        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string FailureMessage { get; set; }

        public string InquiryReferenceNumber { get; set; }

        public bool IsSuccess { get; set; }

        public string ReportId { get; set; }
        public int AttemptCount { get; set; }
    }
}
