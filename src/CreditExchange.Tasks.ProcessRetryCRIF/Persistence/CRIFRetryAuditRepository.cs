﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using CreditExchange.Tasks.ProcessRetryCRIF.Abstractions;

namespace CreditExchange.Tasks.ProcessRetryCRIF.Persistence
{
    public class CRIFRetryAuditRepository : MongoRepository<ICRIFRetryTaskAudit, CRIFRetryTaskAudit>, ICRIFRetryAuditRepository
    {
        static CRIFRetryAuditRepository()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(CRIFRetryTaskAudit)))
            {
                BsonClassMap.RegisterClassMap<CRIFRetryTaskAudit>(map =>
                {
                    map.AutoMap();
                    var type = typeof(CRIFRetryTaskAudit);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
        }
        public CRIFRetryAuditRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "crif-retry-audit")
        {
            CreateIndexIfNotExists("entityType", Builders<ICRIFRetryTaskAudit>.IndexKeys.Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entityId", Builders<ICRIFRetryTaskAudit>.IndexKeys.Ascending(i => i.EntityId));
            CreateIndexIfNotExists("InquiryReferenceNumber", Builders<ICRIFRetryTaskAudit>.IndexKeys.Ascending(i => i.InquiryReferenceNumber));
            CreateIndexIfNotExists("ReportId", Builders<ICRIFRetryTaskAudit>.IndexKeys.Ascending(i => i.ReportId));
        }

        public Task<ICRIFRetryTaskAudit> GetDuplicateRetryEffortDetails(string entityId, string entityType, string tenantId, string inquiryReferenceNumber, string reportId)
        {
            return Task.FromResult<ICRIFRetryTaskAudit>
            (
                Query.First(x => x.EntityId == entityId &&
                                    x.EntityType == entityType &&
                                    x.TenantId == tenantId &&
                                    x.InquiryReferenceNumber == inquiryReferenceNumber
                                    && x.ReportId == reportId)
            );
        }
    }                                         
}
