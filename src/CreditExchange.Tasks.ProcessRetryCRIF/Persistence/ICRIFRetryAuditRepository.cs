﻿using CreditExchange.Tasks.ProcessRetryCRIF.Abstractions;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryCRIF.Persistence
{
    public interface ICRIFRetryAuditRepository : IRepository<ICRIFRetryTaskAudit>
    {
        Task<ICRIFRetryTaskAudit> GetDuplicateRetryEffortDetails(string entityId, string entityType, string tenantId, string inquiryReferenceNumber, string reportId);
    }
}