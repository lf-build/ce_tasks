﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Tasks.ProcessRetryCRIF.Configuration
{
    public class Configuration
    {
        public RetryTaskConfig CRIFRetryConfig { get; set; }
    }
}
