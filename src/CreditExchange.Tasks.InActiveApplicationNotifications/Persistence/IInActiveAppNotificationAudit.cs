﻿using CreditExchange.Tasks.InActiveApplicationNotifications.Abstractions;
using LendFoundry.Foundation.Persistence;

namespace CreditExchange.Tasks.InActiveApplicationNotifications.Persistence
{
    public interface IInActiveAppNotificationAudit : IRepository<IInActiveAppNotificationEntry>
    {
        int GetNotificationCount(string applicationNumber);
    }
}
