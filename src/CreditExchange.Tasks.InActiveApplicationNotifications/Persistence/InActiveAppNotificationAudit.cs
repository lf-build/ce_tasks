﻿using CreditExchange.Tasks.InActiveApplicationNotifications.Abstractions;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Linq;

namespace CreditExchange.Tasks.InActiveApplicationNotifications.Persistence
{
    public class InActiveAppNotificationAudit : MongoRepository<IInActiveAppNotificationEntry, InActiveAppNotificationEntry>, IInActiveAppNotificationAudit
    {       
        public InActiveAppNotificationAudit(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "InActiveAppNotification")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(InActiveAppNotificationEntry)))
            {
                BsonClassMap.RegisterClassMap<InActiveAppNotificationEntry>(map =>
                {
                    map.AutoMap();
                    var type = typeof(InActiveAppNotificationEntry);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            CreateIndexIfNotExists("ApplicationNumber", Builders<IInActiveAppNotificationEntry>.IndexKeys.Ascending(i => i.ApplicationNumber));            
        }

        public int GetNotificationCount(string applicationNumber)
        {
            var result = Query.Where(x => x.ApplicationNumber == applicationNumber && x.TenantId == TenantService.Current.Id).ToList();
            return result != null ? result.Count() : 0;
        }
    }
}
