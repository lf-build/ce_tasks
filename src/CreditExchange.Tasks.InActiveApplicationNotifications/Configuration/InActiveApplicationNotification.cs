﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.InActiveApplicationNotifications
{
    public class InActiveApplicationNotification
    {
        public int InActivityThreshold { get; set; }       
        public IEnumerable<string> NonTerminalApplicationStatus { get; set; }
        public int MaxAttempts { get; set; }
    }
}