using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Tasks.Agent;
using System;
using System.Linq;
using LendFoundry.Foundation.Persistence.Mongo;
using CreditExchange.Tasks.InActiveApplicationNotifications.Persistence;
using LendFoundry.Tenant.Client;
using CreditExchange.Tasks.InActiveApplicationNotifications.Abstractions;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.InActiveApplicationNotifications
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IApplicationFilterClientServiceFactory applicationFilterClientServiceFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IMongoConfiguration mongoConfiguration)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            ApplicationFilterClientServiceFactory = applicationFilterClientServiceFactory;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            TenantServiceFactory = tenantServiceFactory;
            MongoConfiguration = mongoConfiguration;
        }

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IApplicationFilterClientServiceFactory ApplicationFilterClientServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IMongoConfiguration MongoConfiguration { get; }

        #endregion

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName, DateTime.Now.AddHours(1), "InActiveApplicationProcessor", new string[] { });
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var applicationFilters = ApplicationFilterClientServiceFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var tenantService = TenantServiceFactory.Create(reader);
            var InActiveAppNotificationAudit = new InActiveAppNotificationAudit(tenantService, MongoConfiguration);

            var configuration = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Configuration was not found for InActive application notifications");
                return;
            }

            var inActiveApplicationConfiguration = configuration.Get();
            if (inActiveApplicationConfiguration == null)
            {
                logger.Error("Was not found the Configuration related to InActive application notifications");
                return;
            }

            try
            {
                var applications = await applicationFilters.GetAllByStatus(inActiveApplicationConfiguration.InActiveAppNotification.NonTerminalApplicationStatus);
                var applicationsToBeProcessed = applications.TakeWhile(x => tenantTime.Now.Subtract(x.LastProgressDate).TotalDays > inActiveApplicationConfiguration.InActiveAppNotification.InActivityThreshold);

                object sync = new object();

                foreach (var application in applicationsToBeProcessed)
                {
                    logger.Debug($"Process start for ApplicaitonNumber[{application.ApplicationNumber}]");
                    var inActiveDocNotificationCount = InActiveAppNotificationAudit.GetNotificationCount(application.ApplicationNumber);
                    logger.Debug($"InActiveApplicationNotifications Count [{application.ApplicationNumber}] for ApplicaitonNumber[{application.ApplicationNumber}]");
                    if (inActiveDocNotificationCount < inActiveApplicationConfiguration.InActiveAppNotification.MaxAttempts)
                    {
                        lock (sync)
                        {
                            logger.Debug($"InActivityThresholdCrossed event published for [{application.ApplicationNumber}] ");
                            eventHub.Publish("InActivityThresholdCrossed", new { InActiveApplication = application });
                            logger.Debug($"Add applicaiton [{application.ApplicationNumber}] in InActiveAppNotificationAudit ");
                            InActiveAppNotificationAudit.Add(new InActiveAppNotificationEntry() { ApplicationNumber = application.ApplicationNumber, SentOn = new TimeBucket(DateTimeOffset.UtcNow), TenantId = application.TenantId });
                        }
                    }
                }

                //Parallel.ForEach(applicationsToBeProcessed, application =>
                //{
                //    var inActiveDocNotificationCount = InActiveAppNotificationAudit.GetNotificationCount(application.ApplicationNumber);

                //    if (inActiveDocNotificationCount < inActiveApplicationConfiguration.InActiveAppNotification.MaxAttempts)
                //    {
                //        lock (sync)
                //        {
                //            eventHub.Publish("InActivityThresholdCrossed", new { InActiveApplication = application });
                //            InActiveAppNotificationAudit.Add(new InActiveAppNotificationEntry() { ApplicationNumber = application.ApplicationNumber, SentOn = new TimeBucket(DateTimeOffset.UtcNow) , TenantId = application.TenantId  });
                //        }
                //    }
                //});

                logger.Info("end InActive application notifications task");
            }
            catch (Exception ex)
            {
                logger.Error($"Failed attempt to process InActive application notifications.", ex);
            }
        }
    }
}