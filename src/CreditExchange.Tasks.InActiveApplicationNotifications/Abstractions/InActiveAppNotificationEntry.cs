﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Tasks.InActiveApplicationNotifications.Abstractions
{
    public class InActiveAppNotificationEntry : Aggregate, IInActiveAppNotificationEntry
    {
        public string ApplicationNumber { get; set; }

        public TimeBucket SentOn { get; set; }
    }
}
