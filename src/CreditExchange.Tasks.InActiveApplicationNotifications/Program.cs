﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using CreditExchange.Applications.Filters.Client;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.EventHub.Client;
using CreditExchange.Tasks.InActiveApplicationNotifications.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using System;

namespace CreditExchange.Tasks.InActiveApplicationNotifications
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);            
            services.AddApplicationsFilterService(Settings.ApplicationFilter.Host, Settings.ApplicationFilter.Port);
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IInActiveAppNotificationAudit, InActiveAppNotificationAudit>();
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}