using System;
using CreditExchange.Tasks.Agent;
using LendFoundry.Security.Tokens;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Application.Document.Client;
using CreditExchange.Perfios.Client;
using CreditExchange.Perfios.Events;
using LendFoundry.Application.Document;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.SavePerfiosReport
{
    public class SavePerfiosReportAgent : EventBasedAgent
    {
        public SavePerfiosReportAgent
        (
            IConfigurationServiceFactory configurationServiceFactory,
            IEventHubClientFactory eventHubFactory,
            ITokenHandler tokenHandler,
            ITokenReaderFactory tokenHandlerFactory,
            IApplicationDocumentServiceClientFactory applicationDocumentServiceClientFactory,
            IPerfiosServiceClientFactory perfiosServiceClientFactory,
            ILogger logger
            )
            : base(eventHubFactory, nameof(PerfiosXmlReportPulled), tokenHandler, Settings.ServiceName)
        {

            ApplicationDocumentServiceClientFactory = applicationDocumentServiceClientFactory;
            PerfiosServiceClientFactory = perfiosServiceClientFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            Logger = logger;
        }

        #region Variables

        private IPerfiosServiceClientFactory PerfiosServiceClientFactory { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private IApplicationDocumentServiceClientFactory ApplicationDocumentServiceClientFactory { get; }
        private ILogger Logger { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }

        #endregion

        public override async void Execute(EventInfo @event)
        {
            var data = @event.Cast<PerfiosXmlReportPulled>();
            try
            {
                if (data.Response != null)
                {
                    var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var configurationService = ConfigurationServiceFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
                    var configuration = configurationService.Get();
                    if(configuration == null)
                    {
                        Logger.Error($"No configuration found for save perfios report task for tenant #{Tenant}");
                        return;
                    }
                   
                    IApplicationDocumentService applicationdocumentservice = ApplicationDocumentServiceClientFactory.Create(reader);
                    var perfiosService = PerfiosServiceClientFactory.Create(reader);
                    Logger.Info("Retrieve Perfios PDF Document Requested ");
                    var perfiospdfresponse = perfiosService.RetrievePdfReport(data.EntityType, data.EntityId);
                    if (perfiospdfresponse != null)
                    {
                        var filename = data.Name + ".pdf";
                        var response = await applicationdocumentservice.Add(data.EntityId, configuration.DocumentCategoryName, perfiospdfresponse.Result.Report, filename, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Save Perfios PDF Document Failed",ex);
            }
        }
    }
}
