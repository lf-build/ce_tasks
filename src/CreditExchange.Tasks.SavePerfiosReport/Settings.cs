using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Tasks.SavePerfiosReport
{
    public static class Settings
    {
        public static string ServiceName { get; } = "save-perfios-document-task";

        public static string Prefix = "SAVE-PERFIOS-REPORT";

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT_HOST","tenant", $"{Prefix}_TENANT_PORT");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");
        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings($"{Prefix}_APPLICATION_DOCUMENT_HOST", "ce-application-document", $"{Prefix}_APPLICATION_DOCUMENT_PORT");
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings Perfios { get; } = new ServiceSettings($"{Prefix}_PERFIOS_HOST", "syndication-perfios", $"{Prefix}_PERFIOS_PORT");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}