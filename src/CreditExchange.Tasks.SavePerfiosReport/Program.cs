﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.EventHub.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Application.Document.Client;
using CreditExchange.Perfios.Client;
using LendFoundry.Configuration.Client;
using System;

namespace CreditExchange.Tasks.SavePerfiosReport
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddConfigurationService<Configuration.Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddApplicantDocumentService(Settings.ApplicationDocument.Host, Settings.ApplicationDocument.Port);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, SavePerfiosReportAgent>();
            //perfios service endpoint
            services.AddPerfiosService(Settings.Perfios.Host, Settings.Perfios.Port);
            return services;
        }
    }
}