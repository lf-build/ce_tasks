﻿using System.Collections.Generic;

namespace CreditExchange.Tasks.SaveCibilXmlReport.Configuration
{
    public class Configuration
    {
        public IEnumerable<RepositoryConfig> FileUploadConfig { get; set; }        
    }
}
