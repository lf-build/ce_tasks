using System;
using System.IO;
using System.Linq;
using System.Text;
using CreditExchange.Cibil.Client;
using CreditExchange.Syndication.Cibil;
using CreditExchange.Tasks.Agent;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.Application.Document.Client;
using CreditExchange.Sftp;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using System.Threading.Tasks;
using CreditExchange.Tasks.SaveCibilXmlReport.Configuration;
using System.Text.RegularExpressions;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Application.Document;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace CreditExchange.Tasks.SaveCibilXmlReport
{
    public class SaveCibilXmlReportAgent : EventBasedAgent
    {
        public SaveCibilXmlReportAgent(
           IConfigurationServiceFactory configurationServiceFactory,
           IEventHubClientFactory eventHubFactory,
           ITokenHandler tokenHandler,
           ITokenReaderFactory tokenHandlerFactory,
           ICibilReportServiceClientFactory cibilReportServiceClientFactory,
           ILogger logger
        ) : base(eventHubFactory, nameof(cibilreportpulled), tokenHandler, Settings.ServiceName)
        {
            CibilReportServiceClientFactory = cibilReportServiceClientFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            Logger = logger;
        }

        private ICibilReportServiceClientFactory CibilReportServiceClientFactory { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private ILogger Logger { get; }

        private ITokenReaderFactory TokenHandlerFactory { get; }

        public override async void Execute(EventInfo @event)
        {            
            var data = @event.Cast<cibilreportpulled>();
            try
            {
                if (data != null)
                {
                    var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var configurationService = ConfigurationServiceFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
                    var configuration = configurationService.Get();
                    if (configuration == null)
                    {
                        Logger.Error($"No configuration found for save Cibil XML report task for tenant #{Tenant}");
                        return;
                    }

                    var cibilService = CibilReportServiceClientFactory.Create(reader);
                    
                    Logger.Info($"Retrieve Cibil Xml Document Requested for {data.EntityId}");
                    var cibilresponse = cibilService.GetCibilData(data.EntityType, data.EntityId).Result;
                    
                    if (cibilresponse != null)
                    {                        
                        var uploadConfiguration = configuration.FileUploadConfig.SingleOrDefault();
                        if (uploadConfiguration != null)
                        {
                            Logger.Info($"Now processing to save Cibil Xml Report over sftp to {uploadConfiguration.SftpLocationPath}");
                            var fileService = new SftpService(uploadConfiguration.SftpConfiguration, Logger);
                            
                            var filename = data.EntityId + "_CIBILXmlReport.xml";
                            // convert string to stream
                            byte[] byteArray = Encoding.UTF8.GetBytes(cibilresponse.ResponseXML);

                            using (var stream = new MemoryStream(byteArray))
                            {
                                fileService.Upload(stream, filename, uploadConfiguration.SftpLocationPath);
                            }
                        }
                        else
                        {
                            Logger.Error($"Task to save Cibil Xml Report don't have any configuration defined");
                        }                        
                    }
                    else 
                    {
                        Logger.Info($"No Cibil Xml data found for {data.EntityId} from syndication-cibil method GetCibilData");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Save Cibil html Document Failed", ex);
            }
        }        
    }
}
