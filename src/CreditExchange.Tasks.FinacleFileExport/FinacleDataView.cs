﻿using System;

namespace CreditExchange.Tasks.FinacleFileExport
{
    public class FinacleDataView : IFinacleDataView
    {
        public string TypeOfCust { get; set; }
        public string TitleId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MotherMaidenName { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string Qualification { get; set; }
        public string EmploymentStatus { get; set; }
        public string TypeOfCompany { get; set; }
        public string Occupation { get; set; }
        public string SourceOfIncome { get; set; }
        public string GrossIncome { get; set; }
        public string Caste { get; set; }
        public string Community { get; set; }
        public string PanNumber { get; set; }
        public string DocumentCode1 { get; set; }
        public string UniqueId1 { get; set; }
        public string DocumentExpiryDate1 { get; set; }
        public string DocumentCode2 { get; set; }
        public string UniqueId2 { get; set; }
        public string DocumentExpiryDate2 { get; set; }
        public string AddressType1 { get; set; }
        public string AddressLine11 { get; set; }
        public string AddressLine21 { get; set; }
        public string AddressLine31 { get; set; }
        public string City1 { get; set; }
        public string State1 { get; set; }
        public string Pincode1 { get; set; }
        public string AddressType2 { get; set; }
        public string AddressLine12 { get; set; }
        public string AddressLine22 { get; set; }
        public string AddressLine32 { get; set; }
        public string City2 { get; set; }
        public string State2 { get; set; }
        public string Pincode2 { get; set; }
        public string PhoneType1 { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneType2 { get; set; }
        public string PhoneNo2 { get; set; }
        public string EmailType { get; set; }
        public string EmailId { get; set; }
        public string LoanAmount { get; set; }
        public string RateOfInterest { get; set; }
        public string Tenor { get; set; }
        public string EMIAmount { get; set; }
        public string AccountOpeningDate { get; set; }
        public string EMIStartDate { get; set; }
        public string LoanSanctionDate { get; set; }
        public string SanctionRefNo { get; set; }
        public string DocumentDate { get; set; }
        public string RepaymentMethod { get; set; }
        public string OperativeAccountNo { get; set; }
        public string BrokenPeriodInterest { get; set; }
        public string BrokenPeriodIntDueDate { get; set; }
        public string ProcessingFee { get; set; }
        public string PfServiceTax { get; set; }
        public string PfSbSt { get; set; }
        public string PfKkc { get; set; }
        public string NetDisbursementAmt { get; set; }
        public string IfscCode { get; set; }
        public string BeneAccNo { get; set; }
        public string BeneName { get; set; }
        public string BeneAddress1 { get; set; }
    }
}
