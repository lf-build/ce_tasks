using System;
using System.Linq;
using LendFoundry.Configuration.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Tasks.Agent;
using CreditExchange.Applications.Filters.Abstractions.Services;
using System.Collections.Generic;
using System.IO;
using LendFoundry.Clients.DecisionEngine;
using CreditExchange.Sftp;
using LendFoundry.Foundation.Lookup;
using LendFoundry.StatusManagement;
using Newtonsoft.Json.Linq;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.StatusManagement.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.DocumentManager;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.FinacleFileExport
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            IApplicationFilterClientServiceFactory applicationFilterClientFactory,
            IDataAttributesClientFactory dataAttributesEngineFactory,
            ICsvGenerator csvGenerator,
            IDecisionEngineClientFactory decisionEngineServiceFactory,
            ILoggerFactory loggerFactory,
            IFileStorageService fileStorageService,
            IEntityStatusService statusManagementService,
            ILookupClientFactory lookupServiceFactory,
            IStatusManagementServiceFactory statusManagementServiceFactory,
            IDocumentManagerServiceFactory documentServiceFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            ApplicationFilterClientFactory = applicationFilterClientFactory;
            LoggerFactory = loggerFactory;
            CsvGenerator = csvGenerator;
            DataAttributesEngineFactory = dataAttributesEngineFactory;
            DecisionEngineServiceFactory = decisionEngineServiceFactory;
            EventHubFactory = eventHubFactory;
            FileStorageService = fileStorageService;
            LookupServiceFactory = lookupServiceFactory;
            StatusManagementServiceFactory = statusManagementServiceFactory;
            DocumentServiceFactory = documentServiceFactory;
        }

        #region Variables

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IApplicationFilterClientServiceFactory ApplicationFilterClientFactory { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ICsvGenerator CsvGenerator { get; }
        private IDecisionEngineClientFactory DecisionEngineServiceFactory { get; }
        private IDataAttributesClientFactory DataAttributesEngineFactory { get; }
        private IFileStorageService FileStorageService { get; }
        private IStatusManagementServiceFactory StatusManagementServiceFactory { get; }
        private ILookupClientFactory LookupServiceFactory { get; }
        private ILookupService LookupService { get; set; }
        private IDocumentManagerServiceFactory DocumentServiceFactory { get; }

        #endregion

        public override async void OnSchedule()
        {
            var token = TokenHandler.Issue(Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var localTenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            IApplicationFilterService applicationFilter = ApplicationFilterClientFactory.Create(reader);
            var eventHub = EventHubFactory.Create(reader);
            var dataAttributesEngine = DataAttributesEngineFactory.Create(reader);
            var decisionEngineService = DecisionEngineServiceFactory.Create(reader);
            var statusManagementService = StatusManagementServiceFactory.Create(reader);
            var documentManager = DocumentServiceFactory.Create(reader);
            LookupService = LookupServiceFactory.Create(reader);

            var configuration = ConfigurationFactory.Create<Configuration.Configuration>(Settings.ServiceName, reader);
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to application filter Export status Task");
                return;
            }

            var applicationExportConfiguration = configuration.Get();
            if (applicationExportConfiguration == null)
            {
                logger.Error("finacle file export configuration not found.");
                return;
            }
            if (applicationExportConfiguration.CEAprovalExportconfig == null)
            {
                logger.Error("Didn't find Approval status related configuration for finacle file export.");
                return;
            }

            if (string.IsNullOrWhiteSpace(applicationExportConfiguration.CEAprovalExportconfig.ExportTag))
            {
                logger.Warn("No Approval status code found for finaclefile export.");
                return;
            }

            try
            {
                List<string> tags = new List<string>();
                List<string> appsToBeUntagged = new List<string>();
                List<FinacleDataView> batchRecords = new List<FinacleDataView>();
                IDocument uploadedDocument = null;
                tags.Add(applicationExportConfiguration.CEAprovalExportconfig.ExportTag);
                var AllApplicationsToBeExported = await applicationFilter.GetAllByTag(tags);

                logger.Info("Start Export manager task");
                if (AllApplicationsToBeExported != null && AllApplicationsToBeExported.Any())
                {
                    logger.Info("Start for each approved application");
                    foreach (var ApprovedApplication in AllApplicationsToBeExported)
                    {
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(ApprovedApplication.ApplicationNumber))
                            {
                                var dataAttributes = await dataAttributesEngine.GetAllAttributes("application", ApprovedApplication.ApplicationNumber);
                                ReplaceCityNStateCodes(dataAttributes);
                                var statusDetails = await statusManagementService.GetStatusTransitionHistory("application", ApprovedApplication.ApplicationNumber);
                                var loanSanctionStatusCode = applicationExportConfiguration.CEAprovalExportconfig.ApprovedStatusCode;
                                var payload = new { DataAttributes = dataAttributes, LoanSanctionDate = ExtractLoanSanctionDate(statusDetails, loanSanctionStatusCode), ApplicationNumber = ApprovedApplication.ApplicationNumber };
                                if (dataAttributes != null && dataAttributes.Keys.Contains("selectedFinalOffer"))
                                {
                                    var ruleResult = decisionEngineService.Execute<dynamic, FinacleDataView>(applicationExportConfiguration.CEAprovalExportconfig.ExportFinacleRule.Name,
                                                                                                            new { input = payload });

                                    if (ruleResult != null)
                                    {
                                        batchRecords.Add(ruleResult);
                                        appsToBeUntagged.Add(ApprovedApplication.ApplicationNumber);
                                    }
                                    else
                                    {
                                        logger.Error($"Export of finacle record for application {ApprovedApplication.ApplicationNumber} failed.");
                                    }
                                }
                                else
                                {
                                    throw new Exception($"No attributes found for application with number : {ApprovedApplication.ApplicationNumber}");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Warn($"Error occurred while exporting application with identifier {ApprovedApplication.ApplicationNumber}");
                        }
                    }

                    if (batchRecords.Count() > 0)
                    {
                        try
                        {
                            logger.Info("Starting to create finacle file");
                            var csvBytes = CsvGenerator.WriteToCsv(batchRecords, applicationExportConfiguration.CEAprovalExportconfig.CsvProjection);
                            var csvGeneratedFileName = $"FinacleExport_{localTenantTime.Now.ToString("s")}.csv";
                            var docManagerEntityId = Guid.NewGuid().ToString("N");

                            using (var csvMemoryStream = new MemoryStream(csvBytes))
                            {
                                FileStorageService.Upload(csvMemoryStream, csvGeneratedFileName, applicationExportConfiguration.CEAprovalExportconfig.FinacleFileStoragePath);
                            }

                            using (var csvMemoryStream = new MemoryStream(csvBytes))
                            {
                                try
                                {
                                    uploadedDocument = await documentManager.Create(csvMemoryStream, "exportedfiles", docManagerEntityId, csvGeneratedFileName);
                                }
                                catch (Exception ex) { }
                            }

                            foreach (var appNumber in appsToBeUntagged)
                            {
                                applicationFilter.UnTagWithoutEvaluation(appNumber, tags);
                                applicationFilter.TagsWithoutEvaluation(appNumber, new List<string> { applicationExportConfiguration.CEAprovalExportconfig.PostExportTag });
                                if (uploadedDocument != null)
                                {
                                    applicationFilter.AttachFinacleDocumentID(appNumber, docManagerEntityId, uploadedDocument.Id);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error($"Error while processing CSV ", ex);
                            return;
                        }
                        finally
                        {
                            logger.Info("End");
                        }
                    }

                    logger.Info("End for each approved application");
                }
                else
                    logger.Info("No application has been approved yet.");
                logger.Info("end Export manager task");
            }
            catch (Exception ex)
            {
                logger.Error($"Failed attempt to process export to finacle file.", ex);
            }
        }

        private void ReplaceCityNStateCodes(IDictionary<string, object> dataAttributes)
        {
            var applicationObject = JArray.FromObject(dataAttributes["application"]).FirstOrDefault();
            var currentAddress = applicationObject["currentAddress"];
            var city = GetLookupValue("citycode", (string)currentAddress["City"]);
            currentAddress["City"] = city ?? currentAddress["City"];
            var state = GetLookupValue("statecode", (string)currentAddress["State"]);
            currentAddress["State"] = state ?? currentAddress["State"];

            var permanentAddress = applicationObject["permanentAddress"];
            city = GetLookupValue("citycode", (string)permanentAddress["City"]);
            permanentAddress["City"] = city ?? permanentAddress["City"];
            state = GetLookupValue("statecode", (string)permanentAddress["State"]);
            permanentAddress["State"] = state ?? permanentAddress["State"];
            dataAttributes["application"] = new[] { applicationObject };
        }

        private string GetLookupValue(string lookupEntity, string lookupKey)
        {
            var result = LookupService.GetLookupEntry(lookupEntity, lookupKey.ToLower());
            if (result != null)
            {
                return result[lookupKey];
            }
            else
                return null;
        }

        private string ExtractLoanSanctionDate(IEnumerable<IEntityStatus> statusHistory, string ApprovedStatusCode)
        {
            var result = string.Empty;
            var approvalEntry = statusHistory.FirstOrDefault(x => x.Status == ApprovedStatusCode);
            if (approvalEntry != null)
            {
                result = approvalEntry.ActivedOn.Time.ToString("dd-MM-yyyy");
            }
            return result;
        }
    }
}