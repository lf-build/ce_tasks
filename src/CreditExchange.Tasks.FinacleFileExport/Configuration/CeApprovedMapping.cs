﻿using CreditExchange.Sftp.Configuration;
using System.Collections.Generic;

namespace CreditExchange.Tasks.FinacleFileExport.Configuration
{
    public class CeApprovedMapping
    {
        public string ExportTag { get; set; }
        public string PostExportTag { get; set; }
        public string ApprovedStatusCode { get; set; }
        public FinacleFileExport.Configuration.Rule ExportFinacleRule { get; set; }
        public Dictionary<string, string> CsvProjection { get; set; }
        public string FinacleFileStoragePath { get; set; }
        public SftpProviderConfiguration SftpConfiguration { get; set; }
    }
}