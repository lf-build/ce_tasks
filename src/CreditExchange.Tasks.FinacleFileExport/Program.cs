﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.EventHub.Client;
using CreditExchange.Applications.Filters.Client;
using CreditExchange.Sftp.Configuration;
using CreditExchange.Sftp;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.StatusManagement;
using LendFoundry.DocumentManager.Client;
using LendFoundry.DataAttributes.Client;
using System;
using LendFoundry.Configuration;

namespace CreditExchange.Tasks.FinacleFileExport
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);            
            services.AddApplicationsFilterService(Settings.ApplicationFilter.Host, Settings.ApplicationFilter.Port);
            services.AddConfigurationService<Configuration.Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddDocumentManager(Settings.DocumentManager.Host, Settings.DocumentManager.Port);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<ICsvGenerator, CsvGenerator>();
            services.AddTransient<IFileStorageService, SftpService>();
            services.AddTransient<IEntityStatus, EntityStatus>();
                                                    
            services.AddTransient<ISftpProviderConfiguration>(provider =>
            {
                var TokenHandler = provider.GetService<ITokenHandler>();
                var token = TokenHandler.Issue(Settings.Tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                var factory = provider.GetService<IConfigurationServiceFactory<Configuration.Configuration>>();
                var configurationService = factory.Create(reader);
                var result = configurationService.Get();
                var sftpConfiguration = new SftpProviderConfiguration
                {
                    Host = result.CEAprovalExportconfig.SftpConfiguration.Host,
                    Port = result.CEAprovalExportconfig.SftpConfiguration.Port,
                    Username = result.CEAprovalExportconfig.SftpConfiguration.Username,
                    Password = result.CEAprovalExportconfig.SftpConfiguration.Password,
                };
                return sftpConfiguration;
            });

            //services.Add()

            return services;
        }
    }
}