FROM microsoft/dotnet:2.0.6-sdk-2.1.101-stretch AS build-env
ARG RESTORE
RUN : "${RESTORE:?Build argument needs to be set and non-empty.}"
WORKDIR /app

COPY ./src/ /app/
COPY ./Directory.Build.props /app


WORKDIR /app/CreditExchange.Tasks.ProcessRetryPerfios
RUN $RESTORE && dotnet publish -c Release -o out --no-restore

# Build runtime image
FROM microsoft/aspnetcore:2.0.6
WORKDIR /app/
COPY --from=build-env /app/CreditExchange.Tasks.ProcessRetryPerfios/out .

ENTRYPOINT ["dotnet", "CreditExchange.Tasks.ProcessRetryPerfios.dll"]
