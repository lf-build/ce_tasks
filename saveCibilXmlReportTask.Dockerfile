FROM microsoft/dotnet:2.0.6-sdk-2.1.101-stretch AS build-env
ARG RESTORE
WORKDIR /app

COPY ./src/ /app/
RUN find . -name "global.json"  -type f -delete && find . -name "NuGet.config"  -type f -delete && find . -name "*.xproj"  -type f -delete

WORKDIR /app/CreditExchange.Tasks.SaveCibilXmlReport
RUN $RESTORE && dotnet publish --verbosity normal -c Release -o out --no-restore

# Build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/
COPY --from=build-env /app/CreditExchange.Tasks.SaveCibilXmlReport/out .

LABEL lendfoundry.branch "LENDFOUNDRY_BRANCH"
LABEL lendfoundry.commit "LENDFOUNDRY_COMMIT"
LABEL lendfoundry.build "LENDFOUNDRY_BUILD_NUMBER"
LABEL lendfoundry.tag "LENDFOUNDRY_TAG"

ENTRYPOINT ["dotnet", "CreditExchange.Tasks.SaveCibilXmlReport.dll"]